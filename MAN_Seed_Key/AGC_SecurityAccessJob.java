import java.util.Arrays;


public class CFC550EMANSKALGO {

    // quick and dirty, just for testing:
	static{
	    System.load("d:\\develop\\JCFC550E_MAN_SK\\source\\Release\\JCFC550E_MAN_SK.dll");
	}
    // Note:
    // in the SingleEcuJob you can load the DLL in the constructor.
    // do not inherit from "Library". We don't need the JNA functionality


    // we simply define the 3 methods we need:
	native int setDotNetDllPath(String path);
    native byte[] reverseByteArray(byte[] array);
    native byte[] calcKey(byte[] seed);


    // and this is how to use them:
	public static void main(String[] args) {
		CFC550EMANSKALGO dll = new CFC550EMANSKALGO();

		byte[] test = { 47, 11, 0x0d, 0x13, 35 };
		System.out.println("test data:         " + Arrays.toString(test));

		// simple test, which can work without the MAN DLL
		byte[] test2 = dll.reverseByteArray(test);
		System.out.println("reverseByteArray:  " + Arrays.toString(test2));

		// it is necessary to tell the dll, where the MAN DLL is located. Like this:
		dll.setDotNetDllPath("..\\from_MAN\\");

		// now we can calculate the key
		byte[] test3 = dll.calcKey(test);
		System.out.println("calcKey:           " + Arrays.toString(test3));
	}
}
