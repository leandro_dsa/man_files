package de.dsa.manres.eaton.AGC;


public class AGC_SecurityAccessJobEXTEN extends AGC_SecurityAccessJob {

	private static final String		diagCommNameRequestSeed	 					= "DC_SecurAccesRequeSeedFreis1EXTEN";
	private static final String		diagcommNameSendKey		 					= "DC_SecurAccesSendKeyFreis1EXTEN";
	
	private static final String		diagCommNameRequestSeedRspParameterPath		= "PA_Seed.PA_Data";  // Seed Response: Seed ParamPath	
	private static final String		diagcommNameSendKeyRqParameterPathKey		= "PA_Key.PA_Data";	 // SendKey Request:  Key ParamPath
	
	private static final byte		seedSize = 5; // 5 bytes
	private static final byte		keySize	= 5; // 5 bytes
	private static final byte		iSecurityLevel = 0x05;
	private static final byte[]		initKey = {0x02, 0x00, 0x00, 0x00, 0x00};
	
	public String getDiagCommNameRequestSeed() {
		//
		return diagCommNameRequestSeed;
	}
	
	public String getDiagcommNameSendKey() {
		//
		return diagcommNameSendKey;
	}
	
	public String getDiagCommNameRequestSeedRspParameterPath() {
		//
		return diagCommNameRequestSeedRspParameterPath;
	}
	
	public String getDiagcommNameSendKeyRqParameterPathKey() {
		//
		return diagcommNameSendKeyRqParameterPathKey;
	}
	
	protected byte getSeedSize() {
		return seedSize;
	}
	
	protected byte getKeySize() {
		return keySize;
	}

	protected byte getSecurityLevel() {
		return iSecurityLevel;
	}
	
	protected byte[] getInitKey() {
		return initKey;
	}

}
