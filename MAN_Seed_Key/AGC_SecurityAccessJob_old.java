package de.dsa.manres.eaton.AGC;

import java.security.CodeSource;

import com.sun.jna.*;

import de.dsa.manres.MANRES_SecurityAccessJob;
import de.dsa.manres.SAJobHelper;

public class AGC_SecurityAccessJob extends MANRES_SecurityAccessJob {

	private boolean 				accesTypeFixed						= true;
	
	private static final String		dllName								= "CFC550E_MAN_SK.dll";
	private static final String 	variantName                         = "AGC";
	
	private static final byte	sessionInfo								= 0x00;
	private static final byte	seedSize 								= 0; // number of bytes
	private static final byte	keySize									= 0; // number of bytes
	private static final byte	iSecurityLevel							= 0; // 0x01, 0x05, 0x63
	private static final byte[] initKey									= null;
	
	private static boolean	DEBUG 										= false;
	
	
	public boolean accesTypeFixed() {
		return accesTypeFixed;
	}
	
	protected byte getSessionInfo() {
		return sessionInfo;
	}
	
	protected byte getSeedSize() {
		return seedSize;
	}
	
	protected byte getKeySize() {
		return keySize;
	}
	
	protected byte getSecurityLevel() {
		return iSecurityLevel;
	}
	
	protected byte[] getInitKey() {
		return initKey;
	}
	
	public interface AGC_DLLHandler extends Library {
		
		String dllPath = getDLLPath();
		AGC_DLLHandler INSTANCE = (AGC_DLLHandler) Native.loadLibrary(dllPath, AGC_DLLHandler.class);
		
		int CFC550E_MAN_SK_ALGO(Pointer iSeedArray, short iSeedArraySize, Pointer ioKeyArray, int iKeyArraySize, Pointer oSize);
		
	}
	
	private static String getDLLPath() {
		final CodeSource source = AGC_SecurityAccessJob.class.getProtectionDomain().getCodeSource();
		String path = "";
		if (source != null) {
			path = source.getLocation().getPath();		
			if (path.charAt(0) == '/') {
				path = path.replaceFirst("/", "");
			}
			path = path.replaceFirst("MANRES_SecurityAccessJobs.jar", "../ODXLibs/");
			path = path + dllName;
		}
		return path;
	}
	
	public byte[] calcKey(byte[] seed) {
		
		// Set MAN level
		int iSecurityLevel = getSecurityLevel();

		// Set the seed
		Pointer seedPtr = new Memory(seed.length);
		seedPtr.write(0, seed, 0, seed.length);

		// Set the key
		int keyArraySize = getInitKey().length;
		Pointer keyPtr = new Memory(keyArraySize);
		keyPtr.write(0, getInitKey(), 0, keyArraySize);

		// Set output size
		Pointer oSize = new Memory(getKeySize());
		
		// Set the variant
		/** UPDATE: It seems no need to allocate memory for String object, String in Java can be mapped with char* in C. ***/
		//Pointer iVariant = new Memory(16);
		//iVariant.setString(0, variantName);
		
		//
		try{
			AGC_DLLHandler dll = AGC_DLLHandler.INSTANCE;
			
			if(DEBUG) System.out.println("Seed returned: " + SAJobHelper.hexDump(seed));
			
			int result = dll.CFC550E_MAN_SK_ALGO(seedPtr, getSeedSize(), keyPtr, getKeySize(), oSize);

			if(DEBUG) System.out.println("Result is: " + dllCalcKeyResultString(result));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		byte[] key = keyPtr.getByteArray(0, oSize.getInt(0));
		return key;
	}
	
}
