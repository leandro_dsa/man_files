rem @echo off

set ECU_NAME=%1%
set PREFIX=%2%
set BUILD_MASK=%3%
rem set CAN_SEND_ID=%4%
rem set CAN_RECV_ID=%5%
rem set CAN_FUNC_ID=%6%

set IDREF=%7%
set DOCREF=%8%
set DOCTYPE=%9%

set ECU=%ECU_NAME%
set BASEDIR=%~dp0
set JARFILE=%BASEDIR%\jafto.jar

set PYTHONFILE=securities_flash_data.exe %ECU_NAME%

set OUTDIR=..\..\..\AM_General_odx_development\ODX\MODULES\%ECU_NAME%

IF NOT EXIST "%OUTDIR%" mkdir %OUTDIR%
set OUTFILE=%PREFIX%_%ECU_NAME%.odx-d
set OUTFILE_FLASH=FL_%ECU_NAME%.odx-f
set TIMESTAMP=%date:~-4,4%%date:~-7,2%%date:~-10,2%%time:~0,2%%time:~3,2%
set OUTFILEBACKUP="%PREFIX%_%ECU_NAME%_BACKUP_%TIMESTAMP%.odx-d"
set OUTFILEBACKUP_FLASH="FL_%ECU_NAME%_BACKUP_%TIMESTAMP%.odx-f"
set UNITLIB=%BASEDIR%template\ODX_RS_UNIT_LIB.odx-d

set TABLEKEY=TEXT
set FROM=%BASEDIR%..\..\out\%ECU_NAME%\%PREFIX%_%ECU_NAME%.xml
set FROM_CCC=%BASEDIR%..\..\out\%ECU_NAME%\CCC_%PREFIX%_%ECU_NAME%.xml
set TEMPLATE=UDSBV

rem set ODXFILE=%BASEDIR%..\out\%ECU_NAME%%PREFIX%_%ECU_NAME%.odx-d

set /A "BUILD_DTCS=%BUILD_MASK% & 1"
set /A "BUILD_IDS=%BUILD_MASK% & 2"
set /A "BUILD_VALUES=%BUILD_MASK% & 4"
set /A "BUILD_PARAMS=%BUILD_MASK% & 8"
set /A "BUILD_ROUTINES=%BUILD_MASK% & 16"
set /A "BUILD_IOCONTROLS=%BUILD_MASK% & 32"
set /A "BUILD_FLASH=%BUILD_MASK% & 64"

echo Creating a copy of output file for backup
copy /y %OUTDIR%\%OUTFILE% %OUTDIR%\Backup\%OUTFILEBACKUP% > nul
del %OUTDIR%\%OUTFILE%


if %BUILD_DTCS% NEQ 0 (
	echo Create DTC ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,ecu=%ECU%,category=DTC,debug,merge"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
) else (
	set TEMPLATE=UDSBV_NODTCS
	echo Skipping build DTC ODX
)

if %BUILD_IDS% NEQ 0 (
	echo Create Read Identification ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=IDENTIFICATION,readtable=TA_ReadDataByIdentECUIdent,template=%TEMPLATE%,tablekey=%TABLEKEY%,debug,debug,merge,byteoffset=3"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
	
	echo Create Write Identification ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=IDENTIFICATION,template=%TEMPLATE%,writetable=TA_WriteDataByIdentECUIdent,tablekey=%TABLEKEY%,debug,merge,byteoffset=3"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
) else (
	echo Skipping build Identifications ODX for %ECU%
)

if %BUILD_VALUES% NEQ 0 (
	echo Create CCC Values ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM_CCC%,unitfile=%UNITLIB%,ecu=%ECU%,category=VALUE,template=%TEMPLATE%,readtable=TA_ReadDataByIdentECUIdentCCC,tablekey=%TABLEKEY%,debug,merge,byteoffset=3"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
) else (
	echo Skipping build Measurement Values ODX for %ECU%
)

if %BUILD_VALUES% NEQ 0 (
	echo Create Measurement Values ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=VALUE,template=%TEMPLATE%,readtable=TA_ReadDataByIdentMeasuValue,tablekey=%TABLEKEY%,debug,merge,byteoffset=3"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
) else (
	echo Skipping build Measurement Values ODX for %ECU%
)

if %BUILD_PARAMS% NEQ 0 (
	echo Create Parameterization ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=PARAM,template=%TEMPLATE%,readtable=TA_ReadDataByIdentParam,tablekey=%TABLEKEY%,debug,merge,byteoffset=3"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
	
	echo Create Parameterization ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=PARAM,template=%TEMPLATE%,writetable=TA_WriteDataByIdentParam,tablekey=%TABLEKEY%,debug,merge,byteoffset=3"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
) else (
	echo Skipping build Parameterization ODX for %ECU%
)	

if %BUILD_ROUTINES% NEQ 0 (
	echo Create Start Routines ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=ROUTINE,template=%TEMPLATE%,table=TA_RoutiContrStartRoutiRoutiOptio,table2=TA_RoutiContrStartRoutiRoutiStatu,subfunction=START,type=REQUEST,tablekey=%TABLEKEY%,debug,merge,byteoffset=4"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error

	echo Create Stop Routines for %ECU%"
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=ROUTINE,template=%TEMPLATE%,table=TA_RoutiContrStopRoutiRoutiOptio,table2=TA_RoutiContrStopRoutiRoutiStatu,subfunction=STOP,type=REQUEST,tablekey=%TABLEKEY%,debug,merge,byteoffset=4"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error

	echo Create Result Routines for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=ROUTINE,template=%TEMPLATE%,table=TA_RoutiContrRequeRoutiResulRoutiOptio,table2=TA_RoutiContrRequeRoutiResulRoutiStatu,subfunction=REPORT,type=REQUEST,tablekey=%TABLEKEY%,debug,merge,byteoffset=4"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error
) else (
	echo Skipping build Routines ODX for %ECU%
)

if %BUILD_IOCONTROLS% NEQ 0 (
	echo Create IOControl Return Control to ECU ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=IOCONTROL,template=%TEMPLATE%,table=TA_IOContrReturContrToECUIOContrOptio,table2=TA_IOContrReturContrToECUIOContrStatu,subfunction=STOP,type=REQUEST,tablekey=%TABLEKEY%,debug,merge,byteoffset=4"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error

	echo Create IOControl Reset to Default Status for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=IOCONTROL,template=%TEMPLATE%,table=TA_IOContrResetToDefauIOContrOptio,table2=TA_IOContrResetToDefauIOContrStatu,subfunction=RESET,type=REQUEST,tablekey=%TABLEKEY%,debug,merge,byteoffset=4"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error

	echo Create IOControl Freeze Current State ODX for %ECU%
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=IOCONTROL,template=%TEMPLATE%,table=TA_IOContrFreezCurreStateIOContrOptio,table2=TA_IOContrFreezCurreStateIOContrStatu,subfunction=FREEZE,type=REQUEST,tablekey=%TABLEKEY%,debug,merge,byteoffset=4"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error

	echo Create IOControl Short Term Adjustment ODX for %ECU%"
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,ecu=%ECU%,category=IOCONTROL,template=%TEMPLATE%,table=TA_IOContrShortTermAdjusIOContrOptio,table2=TA_IOContrShortTermAdjusIOContrStatu,subfunction=START,type=REQUEST,tablekey=%TABLEKEY%,debug,merge,byteoffset=4"
	if errorlevel 1 echo ##Error
	rem if errorlevel 1 goto error

) else (
	echo Skipping build IOControls ODX for %ECU% %OUTDIR%
)

if %BUILD_FLASH% NEQ 0 (
	echo Create Flash ODX for %ECU%
    rem delete previous file.
	del /Q %OUTDIR%\FL_%ECU%.odx-f  
	rem echo changes
	java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE_FLASH% --afto2odx "aftofile=%FROM%,unitfile=%UNITLIB%,category=ODXF,template=UDSODXF,flashjobref=DC_FlashJob,bvdoc=BV_%ECU%,bvid=BV_%ECU%,avdoc=BV_%ECU%,avid=BV_%ECU%,ecu=%ECU%,odxf,debug,merge"
	if errorlevel 1 echo ##Error

	%PYTHONFILE% %ECU% %OUTDIR%
	if errorlevel 1 echo ##Error creating CRC values in flash files
rem 	if errorlevel 1 goto error
) else (
	echo Skipping build Flash Data ODX for %ECU%
)

set CPTEMPLATE=%BASEDIR%template\comparam-refs_uds.xml
echo Adding ECU information
java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE%  --afto2odx "aftofile=%FROM%,category=ECUINFO,ecu=%ECU%,cptemplate=%CPTEMPLATE%,debug,merge"
if errorlevel 1 echo ##Error
 
rem echo Create Comm Params ODX for %ECU%
rem set BV=BV_%ECU%
rem set CPTEMPLATE=%BASEDIR%template\comparam-refs_uds.xml
rem set TOKENS=__REQCAN__=%CAN_SEND_ID%;__RESPCAN__=%CAN_RECV_ID%;__FUNCAN__=%CAN_FUNC_ID%;__NAME__=%ECU%;
rem java -jar %JARFILE% --basedir %BASEDIR% --outdir %OUTDIR% --outfile %OUTFILE% --comparams "odxfile=%OUTDIR%\%OUTFILE%,bv=%BV%,cptemplate=%CPTEMPLATE%,tokens=%TOKENS%,debug"
rem if errorlevel 1 echo ##Error
rem 
rem echo Clean ODX for %ECU%
rem java -jar %JARFILE% --basedir %BASEDIR% --cleanodx "odxfile=%OUTDIR%/%OUTFILE%,debug"
rem if errorlevel 1 echo ##Error

goto ok

:error
echo  Error occurred. !!!!
goto end

:ok
echo Successfully finished.

:end
