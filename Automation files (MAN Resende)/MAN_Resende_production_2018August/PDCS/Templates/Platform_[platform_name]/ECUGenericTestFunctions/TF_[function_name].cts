<?xml version="1.0" encoding="UTF-8"?>
<xmlExporter version="14.0.0">
  <CtsClass>
    <CtsClassVariant>
      <CtsClassVariant>
        <Chart>
          <Chart>
            <Activity>
              <StartNode>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X04BWKVHI29YNE2LK" />
                    <Target OID="UKN_X1Q460U0J00QW9I5I" />
                    <OID>UKN_X75960U0J00D5KY3U</OID>
                  </Transition>
                </OutgoingTransition>
                <OID>UKN_X04BWKVHI29YNE2LK</OID>
              </StartNode>
              <EndNode>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_X44BWKVHI2GV7C8F2" />
                    <Target OID="UKN_X44BWKVHIQG0CAPNK" />
                    <OID>UKN_X44BWKVHIPG8MAU1Q</OID>
                  </Transition>
                </IncomingTransition>
                <DisplayName>Ende</DisplayName>
                <OID>UKN_X44BWKVHIQG0CAPNK</OID>
              </EndNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>none</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X44BWKVHI5GIAA3R0" />
                            <Target OID="UKN_X44BWKVHIIGTAOP28" />
                            <OID>UKN_XWX1B37SI00I93228</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X44BWKVHI5GIAA3R0</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XWX1B37SI00I93228" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X44BWKVHIIGTAOP28</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X44BWKVHI3G0F1RY0</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_XX5450U0J10ZCPAY0" />
                    <Target OID="UKN_X44BWKVHI2GV7C8F2" />
                    <OID>UKN_X19750U0J00YTEE6E</OID>
                  </Transition>
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_X44BWKVHIPG8MAU1Q" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Finalization</DisplayName>
                <OID>UKN_X44BWKVHI2GV7C8F2</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X14BWKVHINABXL8UA" />
                            <Target OID="UKN_X14BWKVHIBB5G1VEB" />
                            <OID>UKN_XP32ZKVHIACPYIDM9</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X14BWKVHINABXL8UA</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XP32ZKVHIACPYIDM9" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X14BWKVHIBB5G1VEB</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X14BWKVHILAZBSS50</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>if</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_X1Q460U0J00QW9I5I" />
                    <Target OID="UKN_X14BWKVHIKAEZUJYL" />
                    <OID>UKN_XBTB60U0J00W8W431</OID>
                  </Transition>
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X14BWKVHIKAEZUJYL" />
                    <Target OID="UKN_XX5450U0J00FQW6XD" />
                    <OID>UKN_XZH550U0J00SMZNT6</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>[--insert test here--]</DisplayName>
                <OID>UKN_X14BWKVHIKAEZUJYL</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>none</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XX5450U0JI0W79CCU" />
                            <Target OID="UKN_XX5450U0JJ02KKLLY" />
                            <OID>UKN_XX5450U0JK0GLYQ7D</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XX5450U0JI0W79CCU</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_XX5450U0JJ02KKLLY" />
                            <Target OID="UKN_XX5450U0JL0EOR345" />
                            <OID>UKN_XX5450U0JM0UR307U</OID>
                          </Transition>
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XX5450U0JL0EOR345</OID>
                      </CompoundOut>
                      <BuildingBlock>
                        <BuildingBlockClass>
                          <Reference oid="BBC_X64PN7U3I0CU5FYDE" type="BuildingBlockClass" />
                        </BuildingBlockClass>
                        <ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <OrderNo>0</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                                </DataType>
                                <DisplayName>logicalLink</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_XK5ZN7U3IEC5Y8AV8</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <FormalParameterSymbol>
                              <LocalReference oid="UKN_X04BWKVHIB9JXJSAA" type="FormalParameter" />
                            </FormalParameterSymbol>
                            <DisplayName>1.logicalLink.f.f</DisplayName>
                            <OID>UKN_XX5450U0JN0MI54DZ</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <OrderNo>1</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>status</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_XCRAO7U3IICWQSWXJ</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>OK</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_XX5450U0JP039Q8TG</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.status.f.f</DisplayName>
                            <OID>UKN_XX5450U0JO0GX4SNY</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XBG550U0J00EPVIFI" type="Constant" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <OrderNo>2</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>action</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_X850P7U3I8WVRNDWX</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <DisplayName>1.action.f.f</DisplayName>
                            <OID>UKN_XX5450U0JQ00F61JY</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <IsOptional>true</IsOptional>
                                <OrderNo>3</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>progress</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_XTF0PLV3I42528O7N</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>100</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_XX5450U0JS02NDZ80</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.progress.f.f</DisplayName>
                            <OID>UKN_XX5450U0JR0K8C9WK</OID>
                          </ActualParameter>
                        </ActualParameter>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XX5450U0JK0GLYQ7D" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_XX5450U0JM0UR307U" type="Transition" />
                        </OutgoingTransition>
                        <DisplayName>Report OK</DisplayName>
                        <OID>UKN_XX5450U0JJ02KKLLY</OID>
                      </BuildingBlock>
                    </Activity>
                    <OID>UKN_XX5450U0JG04HMGJP</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>if</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XZH550U0J00SMZNT6" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XX5450U0J00FQW6XD" />
                    <Target OID="UKN_XX5450U0J10ZCPAY0" />
                    <OID>UKN_XX5450U0J20DCOEJW</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test: OK</DisplayName>
                <OID>UKN_XX5450U0J00FQW6XD</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>none</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XX5450U0J50Z71LMM" />
                            <Target OID="UKN_XX5450U0J60J14MYR" />
                            <OID>UKN_XX5450U0J70MSNP3N</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XX5450U0J50Z71LMM</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_XX5450U0J60J14MYR" />
                            <Target OID="UKN_XX5450U0J80IALANQ" />
                            <OID>UKN_XX5450U0J90YLYHQH</OID>
                          </Transition>
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XX5450U0J80IALANQ</OID>
                      </CompoundOut>
                      <BuildingBlock>
                        <BuildingBlockClass>
                          <Reference oid="BBC_X64PN7U3I0CU5FYDE" type="BuildingBlockClass" />
                        </BuildingBlockClass>
                        <ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XK5ZN7U3IEC5Y8AV8" type="FormalParameter" />
                            </FormalParameter>
                            <FormalParameterSymbol>
                              <LocalReference oid="UKN_X04BWKVHIB9JXJSAA" type="FormalParameter" />
                            </FormalParameterSymbol>
                            <DisplayName>1.logicalLink.f.f</DisplayName>
                            <OID>UKN_XX5450U0JA0UQGKGU</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XCRAO7U3IICWQSWXJ" type="FormalParameter" />
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>NOK</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_XX5450U0JC05MPVWM</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.status.f.f</DisplayName>
                            <OID>UKN_XX5450U0JB0MKPA6L</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XBG550U0J00EPVIFI" type="Constant" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <LocalReference oid="UKN_X850P7U3I8WVRNDWX" type="FormalParameter" />
                            </FormalParameter>
                            <DisplayName>1.action.f.f</DisplayName>
                            <OID>UKN_XX5450U0JD05P5IST</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XTF0PLV3I42528O7N" type="FormalParameter" />
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>100</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_XX5450U0JF0XLLP14</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.progress.f.f</DisplayName>
                            <OID>UKN_XX5450U0JE0Q0N8R8</OID>
                          </ActualParameter>
                        </ActualParameter>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XX5450U0J70MSNP3N" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_XX5450U0J90YLYHQH" type="Transition" />
                        </OutgoingTransition>
                        <DisplayName>Report NOK</DisplayName>
                        <OID>UKN_XX5450U0J60J14MYR</OID>
                      </BuildingBlock>
                    </Activity>
                    <OID>UKN_XX5450U0J3029VFZ2</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>prog</CondExecCondition>
                <CondExecMode>if</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <CondExecCode>returnValues.getReturnValue() != OK</CondExecCode>
                <IncomingTransition>
                  <LocalReference oid="UKN_XX5450U0J20DCOEJW" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_X19750U0J00YTEE6E" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Test: Not OK</DisplayName>
                <OID>UKN_XX5450U0J10ZCPAY0</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X1Q460U0J30BXUI58" />
                            <Target OID="UKN_X1Q460U0J60R6TRMY" />
                            <OID>UKN_XQW2R0U0J00UT8KKC</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X1Q460U0J30BXUI58</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_X1Q460U0J60R6TRMY" />
                            <Target OID="UKN_X1Q460U0J70Z2R1UT" />
                            <OID>UKN_X1Q460U0J804X4PI8</OID>
                          </Transition>
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X1Q460U0J70Z2R1UT</OID>
                      </CompoundOut>
                      <BuildingBlock>
                        <BuildingBlockClass>
                          <Reference oid="BBC_X64PN7U3I0CU5FYDE" type="BuildingBlockClass" />
                        </BuildingBlockClass>
                        <ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XK5ZN7U3IEC5Y8AV8" type="FormalParameter" />
                            </FormalParameter>
                            <FormalParameterSymbol>
                              <LocalReference oid="UKN_X04BWKVHIB9JXJSAA" type="FormalParameter" />
                            </FormalParameterSymbol>
                            <DisplayName>1.logicalLink.f.f</DisplayName>
                            <OID>UKN_X1Q460U0JA0E6AT6C</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XD4960U0JQ25L88T6" type="Variable" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <LocalReference oid="UKN_XCRAO7U3IICWQSWXJ" type="FormalParameter" />
                            </FormalParameter>
                            <DisplayName>1.status.f.f</DisplayName>
                            <OID>UKN_X1Q460U0JB08IP4F3</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XBG550U0J00EPVIFI" type="Constant" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <LocalReference oid="UKN_X850P7U3I8WVRNDWX" type="FormalParameter" />
                            </FormalParameter>
                            <DisplayName>1.action.f.f</DisplayName>
                            <OID>UKN_X1Q460U0JC0XSQ9EC</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XTF0PLV3I42528O7N" type="FormalParameter" />
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>0</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_X1Q460U0JE0BZTKXX</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.progress.f.f</DisplayName>
                            <OID>UKN_X1Q460U0JD0WOT4P7</OID>
                          </ActualParameter>
                        </ActualParameter>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XQW2R0U0J00UT8KKC" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_X1Q460U0J804X4PI8" type="Transition" />
                        </OutgoingTransition>
                        <OID>UKN_X1Q460U0J60R6TRMY</OID>
                      </BuildingBlock>
                    </Activity>
                    <OID>UKN_X1Q460U0J10YQC2HR</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X75960U0J00D5KY3U" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_XBTB60U0J00W8W431" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Initialization</DisplayName>
                <OID>UKN_X1Q460U0J00QW9I5I</OID>
              </StructureNode>
            </Activity>
            <OID>UKN_X04BWKVHI093587YM</OID>
          </Chart>
        </Chart>
        <FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X1Q460U0J00QW9I5I" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>In the initialisation part, the ECU Overview is updated with the name of performed test and the information that the test is in progress.</Data>
                <OID>UKN_XFGBC0U0J10N6O09T</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XFGBC0U0J00VOJJB6</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X44BWKVHI2GV7C8F2" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Optional, can be removed if not needed.</Data>
                <OID>UKN_XL23D0U0J10H333SG</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XL23D0U0J00TNPPG4</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XX5450U0J00FQW6XD" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Test Result&#xD;
&#xD;
In this structure node you can execute actions that are not part of the actual test anymore, but depend on the result of the test, such as updating an overview for the user. It is important to assure that the return value of the whole test module still reflects the result of the test. This has to be assured by setting the assessment of the structure node to none for both cases and also a possible finalization part.</Data>
                <OID>UKN_XV09E0U0J1079JDR1</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XV09E0U0J00ZDV6HD</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X14BWKVHIKAEZUJYL" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Structure your Test&#xD;
&#xD;
Enter the section [-- insert Test Here--].  Use Structure nodes to create the steps of your sequence.</Data>
                <OID>UKN_XT5UF0U0J102S1ADX</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XT5UF0U0J004GL8BU</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X14BWKVHIKAEZUJYL" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Add Statistics&#xD;
&#xD;
A generic test function is required to contain the following statistics:&#xD;
&#xD;
ECU Context as Formal Parameter:  The ECU context is received from the outside as a formal parameter.  For generic Test Functions it should not be hardcoded here&#xD;
&#xD;
Function Context:  Open the Statistic Hierarchy explorer and drag and drop a Statistic Context of Level "FUNCTION" for this generic test function.  The parent of this Statistic Context must be the ECU Context.&#xD;
&#xD;
Statistic Steps&#xD;
Typically, one statistic step is required for each Test Step.  &#xD;
&#xD;
Drag and drop the desired statistics from the Statistics hierarchy explorer.</Data>
                <OID>UKN_XI4FI0U0J10SIBXJK</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XI4FI0U0J00ER5KU2</OID>
          </FloatingComment>
        </FloatingComment>
        <LocalVariable>
          <StructValue>
            <StructElement>
              <StructElement>
                <DataElement>
                  <SymbolValue>
                    <FormalParameterSymbol>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>1</OrderNo>
                        <DataType>
                          <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                        </DataType>
                        <DisplayName>ecuContext</DisplayName>
                        <Description>Ecu statistical context used to generate ecu specific statistics</Description>
                        <OID>UKN_X14BWKVHIXAVQEBS2</OID>
                      </FormalParameter>
                    </FormalParameterSymbol>
                    <DataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_X1Q890U0J102G38L8</OID>
                  </SymbolValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </MemberDataType>
                    <DisplayName>parent</DisplayName>
                    <Description>NO DESCRIPTION</Description>
                    <OID>UKN_MEMBERPARENT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_X1Q890U0J00Z823MR</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <StatisticContextValue>
                    <StatisticContext>
                      <Reference oid="STC_XY4S6IJJI50KB0B63" type="StatisticContext" />
                    </StatisticContext>
                    <DataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </DataType>
                    <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                    <OID>UKN_X1Q890U0J300VIT1X</OID>
                  </StatisticContextValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>statisticContextEntity</DisplayName>
                    <Description>The statistic context</Description>
                    <OID>UKN_MEMBERSTATCNTXT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_X1Q890U0J20CAMQYE</OID>
              </StructElement>
            </StructElement>
            <DataType>
              <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
            </DataType>
            <DisplayName>functionContext</DisplayName>
            <Description>The generic function context used for this test</Description>
            <OID>UKN_X14BWKVHIUAKBV8L5</OID>
          </StructValue>
          <Variable>
            <Value>de.dsa.[customer].[...].[function_name]</Value>
            <DataType>
              <Reference oid="SDT_ILogger" type="ExternalDataType" />
            </DataType>
            <DisplayName>LOG</DisplayName>
            <Description>ILogger variable, path has to be adapted</Description>
            <OID>UKN_XS82HMVHIEEP8WG71</OID>
          </Variable>
          <Constant>
            <IsConstant>true</IsConstant>
            <Value>10_dtc</Value>
            <DataType>
              <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
            </DataType>
            <DisplayName>DTCREAD</DisplayName>
            <Description>Example for a test function identifier, value is name of corresponding icon</Description>
            <OID>UKN_XBG550U0J00EPVIFI</OID>
          </Constant>
          <Variable>
            <Value>IN_PROGRESS</Value>
            <DataType>
              <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
            </DataType>
            <DisplayName>IN_PROGRESS</DisplayName>
            <Description>Can be send to ECU overview to provide feedback that the test is currently being performed</Description>
            <OID>UKN_XD4960U0JQ25L88T6</OID>
          </Variable>
        </LocalVariable>
        <OID>CTc_XZ3BWKVHIY8W0H00B</OID>
        <Completeness>incomplete</Completeness>
        <Description>Variant</Description>
        <RestrictionStatus>UNKNOWN</RestrictionStatus>
      </CtsClassVariant>
    </CtsClassVariant>
    <FormalParameter>
      <FormalParameter>
        <ParameterDirection>in</ParameterDirection>
        <OrderNo>0</OrderNo>
        <DataType>
          <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
        </DataType>
        <DisplayName>logicalLink</DisplayName>
        <Description>Logical link for the ECU that the test should be performed on</Description>
        <OID>UKN_X04BWKVHIB9JXJSAA</OID>
      </FormalParameter>
      <LocalReference oid="UKN_X14BWKVHIXAVQEBS2" type="FormalParameter" />
    </FormalParameter>
    <ScriptingCategory>testfunction</ScriptingCategory>
    <TestModuleAcceptanceStatus>no_status</TestModuleAcceptanceStatus>
    <VisualizationType>standard</VisualizationType>
    <DisplayName>TF_[function_name]</DisplayName>
    <Description>Template for a Generic Test Function.&#xD;
A generic test function implements a test that can be re-used between different ECUs. It therefore needs the ECU context as an input parameter.&#xD;
&#xD;
Follow the instructions on the comments tab to fully implement this test function.&#xD;
</Description>
    <Completeness>complete</Completeness>
    <OID>CTc_XZ3BWKVHIO8WQROUP</OID>
  </CtsClass>
</xmlExporter>

