<?xml version="1.0" encoding="UTF-8"?>
<xmlExporter version="14.0.0">
  <CtsClass>
    <CtsClassVariant>
      <CtsClassVariant>
        <Chart>
          <Chart>
            <Activity>
              <StartNode>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XX8VXJECIICRX8ZXX" />
                    <Target OID="UKN_XTYWYJECIUWU9US2M" />
                    <OID>UKN_XQZOQANCI3R48E81Q</OID>
                  </Transition>
                </OutgoingTransition>
                <OID>UKN_XX8VXJECIICRX8ZXX</OID>
              </StartNode>
              <EndNode>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_XX8VXJECILCX2Z6C8" />
                    <OID>UKN_XPXPQANCI7TVTHM0C</OID>
                  </Transition>
                </IncomingTransition>
                <DisplayName>End</DisplayName>
                <OID>UKN_XX8VXJECILCX2Z6C8</OID>
              </EndNode>
              <BuildingBlock>
                <BuildingBlockClass>
                  <Reference oid="BBC_X14Q5LP9IQRCYBL7H" type="BuildingBlockClass" />
                </BuildingBlockClass>
                <ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XCAOBBJ0J00X8K189" type="ComplexExternalValue" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>0</OrderNo>
                        <DataType>
                          <Reference oid="SDT_ILocation" type="ComplexExternalDataType" />
                        </DataType>
                        <DisplayName>location</DisplayName>
                        <Description>The ILocation of the ECU</Description>
                        <OID>UKN_X24Q5LP9IXR57WO4C</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.location.f.f</DisplayName>
                    <OID>UKN_XTYWYJECIXWDBFUNG</OID>
                  </ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XJNSZJECIF23XVU9G" type="StructValue" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>out</ParameterDirection>
                        <OrderNo>1</OrderNo>
                        <DataType>
                          <Reference oid="SDT_XXJ0RG37I8KJRICX3" type="StructDataType" />
                        </DataType>
                        <DisplayName>ecuRuntime</DisplayName>
                        <Description>The ECURuntimeData</Description>
                        <OID>UKN_X24Q5LP9I0SS2G6CT</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>2.ecuRuntime.f.f</DisplayName>
                    <OID>UKN_XTYWYJECI0XGTEBME</OID>
                  </ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XMNSZJECII2XRZBBJ" type="Variable" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>out</ParameterDirection>
                        <OrderNo>2</OrderNo>
                        <DataType>
                          <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                        </DataType>
                        <DisplayName>logicalLink</DisplayName>
                        <Description>The RTLogicalLink in use</Description>
                        <OID>UKN_X24Q5LP9I3SCUAADF</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>2.logicalLink.f.f</DisplayName>
                    <OID>UKN_XTYWYJECI3X7WRDLV</OID>
                  </ActualParameter>
                </ActualParameter>
                <IncomingTransition>
                  <LocalReference oid="UKN_XQZOQANCI3R48E81Q" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XTYWYJECIUWU9US2M" />
                    <Target OID="UKN_XED34BJ0J00U3WTC9" />
                    <OID>UKN_XH294BJ0J0042T0B8</OID>
                  </Transition>
                </OutgoingTransition>
                <OID>UKN_XTYWYJECIUWU9US2M</OID>
              </BuildingBlock>
              <CtsDecision>
                <DecisionMode>firstmatch</DecisionMode>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_XED34BJ0J00U3WTC9" />
                    <Target OID="UKN_XCBF5KECIE80B1T2W" />
                    <OID>UKN_XT294BJ0J00YOH62K</OID>
                  </Transition>
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_X2WF5KECIHCYC5NDY" />
                    <OID>UKN_X93G5KECIMCOTWSKF</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_XT927KECIHFTK8FA5" />
                    <OID>UKN_XYU37KECIKF6RWS2P</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_XUSDJQECIGZ92H30C" />
                    <OID>UKN_XF6EJQECIJZJHF341</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_X2GFYQECIJHXGZBOP" />
                    <OID>UKN_X9MFYQECI3IM2OZJJ</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_X0MJ0RECIWH6P58PI" />
                    <OID>UKN_XDRJ0RECIZH5TRK7B</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_XNZH1RECIEC57NT3X" />
                    <OID>UKN_X65I1RECIHCGDYEJ8</OID>
                  </Transition>
                  <LocalReference oid="UKN_XPXPQANCI7TVTHM0C" type="Transition" />
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_XB4O6BJ0J00NQTKMJ" />
                    <OID>UKN_XC4O6BJ0J005Z6DEO</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XCBF5KECIE80B1T2W" />
                    <Target OID="UKN_XEDT6BJ0J00VH923F" />
                    <OID>UKN_XFDT6BJ0J00RWA2YB</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>First Match</DisplayName>
                <OID>UKN_XCBF5KECIE80B1T2W</OID>
              </CtsDecision>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>1</ConditionOrder>
                <Condition>command.equals("CONNECTION_OPEN")</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X93G5KECIMCOTWSKF" type="Transition" />
                </IncomingTransition>
                <DisplayName>CONNECTION_OPEN</DisplayName>
                <OID>UKN_X2WF5KECIHCYC5NDY</OID>
              </DecisionCondition>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>2</ConditionOrder>
                <Condition>command.equals("CONNECTION_CLOSE")</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XYU37KECIKF6RWS2P" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XT927KECIHFTK8FA5" />
                    <Target OID="UKN_XEVVZPECIEC7QT274" />
                    <OID>UKN_X94SQANCI7F1LOCCH</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>CONNECTION_CLOSE</DisplayName>
                <OID>UKN_XT927KECIHFTK8FA5</OID>
              </DecisionCondition>
              <Cts>
                <CtsClass>
                  <Reference oid="CTc_X52ZSPECI0ZBVG6I5" type="CtsClass" />
                </CtsClass>
                <CtsClassOIDString>CTc_X52ZSPECI0ZBVG6I5</CtsClassOIDString>
                <ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XMNSZJECII2XRZBBJ" type="Variable" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>0</OrderNo>
                        <DataType>
                          <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                        </DataType>
                        <DisplayName>logicalLink</DisplayName>
                        <Description>Logical link used to communicate with the given location</Description>
                        <OID>UKN_X82ZSPECI10LVWU47</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.logicalLink.f.f</DisplayName>
                    <OID>UKN_X6GH0QECI0KRTZS90</OID>
                  </ActualParameter>
                </ActualParameter>
                <IncomingTransition>
                  <LocalReference oid="UKN_X94SQANCI7F1LOCCH" type="Transition" />
                </IncomingTransition>
                <OID>UKN_XEVVZPECIEC7QT274</OID>
              </Cts>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>3</ConditionOrder>
                <Condition>command.equals("ASSEMBLY_CHECK")</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XF6EJQECIJZJHF341" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XUSDJQECIGZ92H30C" />
                    <Target OID="UKN_X5CHSRVHIY4PGXKV4" />
                    <OID>UKN_XKSISRVHIF6T8TVRD</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>ASSEMBLY_CHECK</DisplayName>
                <OID>UKN_XUSDJQECIGZ92H30C</OID>
              </DecisionCondition>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>4</ConditionOrder>
                <Condition>command.equals("READ_DTC")</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X9MFYQECI3IM2OZJJ" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X2GFYQECIJHXGZBOP" />
                    <Target OID="UKN_X42PJRECIINCF0KBN" />
                    <OID>UKN_X7TTQANCI5NCDZFC9</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>READ_DTC</DisplayName>
                <OID>UKN_X2GFYQECIJHXGZBOP</OID>
              </DecisionCondition>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>5</ConditionOrder>
                <Condition>command.equals("DELETE_DTC")</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XDRJ0RECIZH5TRK7B" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X0MJ0RECIWH6P58PI" />
                    <Target OID="UKN_XROFORECI7YNBPGG0" />
                    <OID>UKN_XOFUQANCIF2NM1THV</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>DELETE_DTC</DisplayName>
                <OID>UKN_X0MJ0RECIWH6P58PI</OID>
              </DecisionCondition>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>8</ConditionOrder>
                <Condition>true</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X65I1RECIHCGDYEJ8" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XNZH1RECIEC57NT3X" />
                    <Target OID="UKN_XBMJ2RECIAZ64PV3R" />
                    <OID>UKN_XOFWQANCIJHIFUEAI</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>ERROR</DisplayName>
                <OID>UKN_XNZH1RECIEC57NT3X</OID>
              </DecisionCondition>
              <BuildingBlock>
                <BuildingBlockClass>
                  <Reference oid="BBC_XZF5CO17I3HQHGCBC" type="BuildingBlockClass" />
                </BuildingBlockClass>
                <ActualParameter>
                  <ActualParameter>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>out</ParameterDirection>
                        <IsOptional>true</IsOptional>
                        <OrderNo>0</OrderNo>
                        <DataType>
                          <Reference oid="SDT_0000000000000002" type="BasicDataType" />
                        </DataType>
                        <DisplayName>result</DisplayName>
                        <Description>The result value to be returned</Description>
                        <OID>UKN_XE3JPIB7IB15TX0JF</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>2.result.f.f</DisplayName>
                    <OID>UKN_XBMJ2RECIDZWZBHW2</OID>
                  </ActualParameter>
                </ActualParameter>
                <IncomingTransition>
                  <LocalReference oid="UKN_XOFWQANCIJHIFUEAI" type="Transition" />
                </IncomingTransition>
                <OID>UKN_XBMJ2RECIAZ64PV3R</OID>
              </BuildingBlock>
              <Cts>
                <CtsClass>
                  <Reference oid="CTc_X1QN7RECI9YB7QBLY" type="CtsClass" />
                </CtsClass>
                <CtsClassOIDString>CTc_X1QN7RECI9YB7QBLY</CtsClassOIDString>
                <ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XMNSZJECII2XRZBBJ" type="Variable" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>0</OrderNo>
                        <DataType>
                          <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                        </DataType>
                        <DisplayName>logicallink</DisplayName>
                        <Description>The logical link that will be tested</Description>
                        <OID>UKN_X2QN7RECIWYYGY240</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.logicallink.f.f</DisplayName>
                    <OID>UKN_X42PJRECILN7ULCSA</OID>
                  </ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_X334PPECIN74SOIZH" type="StructValue" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>1</OrderNo>
                        <DataType>
                          <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                        </DataType>
                        <DisplayName>ecuContext</DisplayName>
                        <Description>The statistics Context for the ECU</Description>
                        <OID>UKN_X6QN7RECIB2A0XWCR</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.ecuContext.f.f</DisplayName>
                    <OID>UKN_X42PJRECIONWXL6UW</OID>
                  </ActualParameter>
                  <ActualParameter>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in_out</ParameterDirection>
                        <IsOptional>true</IsOptional>
                        <OrderNo>2</OrderNo>
                        <DataType>
                          <Reference oid="SDT_RTDtcList" type="ExternalDataType" />
                        </DataType>
                        <DisplayName>listOfReadDtcs</DisplayName>
                        <Description>The list of DTCs retrieved from the read execution</Description>
                        <OID>UKN_X4QN7RECIA0FJ6P7I</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>3.listOfReadDtcs.f.f</DisplayName>
                    <OID>UKN_X42PJRECIRNPUHLNY</OID>
                  </ActualParameter>
                </ActualParameter>
                <IncomingTransition>
                  <LocalReference oid="UKN_X7TTQANCI5NCDZFC9" type="Transition" />
                </IncomingTransition>
                <OID>UKN_X42PJRECIINCF0KBN</OID>
              </Cts>
              <Cts>
                <CtsClass>
                  <Reference oid="CTc_XRMDLRECIV7QB5UOZ" type="CtsClass" />
                </CtsClass>
                <CtsClassOIDString>CTc_XRMDLRECIV7QB5UOZ</CtsClassOIDString>
                <ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XMNSZJECII2XRZBBJ" type="Variable" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>0</OrderNo>
                        <DataType>
                          <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                        </DataType>
                        <DisplayName>logicallink</DisplayName>
                        <Description>The logical link that will be tested</Description>
                        <OID>UKN_XSMDLRECIO8XKR17C</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.logicallink.f.f</DisplayName>
                    <OID>UKN_X38KFRRHIVQX5U47E</OID>
                  </ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_X334PPECIN74SOIZH" type="StructValue" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>1</OrderNo>
                        <DataType>
                          <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                        </DataType>
                        <DisplayName>ecuContext</DisplayName>
                        <Description>The statistics Context for the ECU</Description>
                        <OID>UKN_XVMDLRECIIC4F1CVV</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.ecuContext.f.f</DisplayName>
                    <OID>UKN_X38KFRRHIYQCR392Y</OID>
                  </ActualParameter>
                </ActualParameter>
                <IncomingTransition>
                  <LocalReference oid="UKN_XOFUQANCIF2NM1THV" type="Transition" />
                </IncomingTransition>
                <OID>UKN_XROFORECI7YNBPGG0</OID>
              </Cts>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X5CHSRVHI25K2WB25" />
                            <Target OID="UKN_X5CHSRVHI55WB8DB1" />
                            <OID>UKN_X5CHSRVHI85WJNT5M</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X5CHSRVHI25K2WB25</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X5CHSRVHI85WJNT5M" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X5CHSRVHI55WB8DB1</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X5CHSRVHI159PM0V6</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XKSISRVHIF6T8TVRD" type="Transition" />
                </IncomingTransition>
                <DisplayName>TF_AssemblyCheck</DisplayName>
                <OID>UKN_X5CHSRVHIY4PGXKV4</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XFD34BJ0J80GQFVWD" />
                            <Target OID="UKN_XFD34BJ0J90I1WY05" />
                            <OID>UKN_XFD34BJ0JA06I5PD0</OID>
                          </Transition>
                        </OutgoingTransition>
                        <OID>UKN_XFD34BJ0J80GQFVWD</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XFD34BJ0JA06I5PD0" type="Transition" />
                        </IncomingTransition>
                        <OID>UKN_XFD34BJ0J90I1WY05</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XFD34BJ0J60JNLOC8</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>prog</CondExecCondition>
                <CondExecMode>if</CondExecMode>
                <CtsClass>
                  <Reference oid="CTc_X3JBZKECI8YWTVQJF" type="CtsClass" />
                </CtsClass>
                <AssessmentCondition>0</AssessmentCondition>
                <CondExecCode>(logicalLink == null || !logicalLink.isValid())</CondExecCode>
                <ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XCAOBBJ0J00X8K189" type="ComplexExternalValue" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>0</OrderNo>
                        <DataType>
                          <Reference oid="SDT_ILocation" type="ComplexExternalDataType" />
                        </DataType>
                        <DisplayName>location</DisplayName>
                        <Description>The Location of the ECU to which the Logical Link will be opened</Description>
                        <OID>UKN_XN371PECITX6KTZTC</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.location.f.f</DisplayName>
                    <OID>UKN_XED34BJ0J105GOG5A</OID>
                  </ActualParameter>
                  <ActualParameter>
                    <DataElementSymbol>
                      <LocalReference oid="UKN_XMNSZJECII2XRZBBJ" type="Variable" />
                    </DataElementSymbol>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>out</ParameterDirection>
                        <OrderNo>1</OrderNo>
                        <DataType>
                          <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                        </DataType>
                        <DisplayName>logicalLink</DisplayName>
                        <Description>Logical link used to communicate with the given location</Description>
                        <OID>UKN_XZQR7LECIHNHNCQB1</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>2.logicalLink.f.f</DisplayName>
                    <OID>UKN_XFD34BJ0J00B5MUID</OID>
                  </ActualParameter>
                  <ActualParameter>
                    <FormalParameter>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <IsOptional>true</IsOptional>
                        <OrderNo>2</OrderNo>
                        <DataType>
                          <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                        </DataType>
                        <DisplayName>ecuContext</DisplayName>
                        <Description>The ECU context to be used</Description>
                        <OID>UKN_XUNMV64EIEZIIQDZP</OID>
                      </FormalParameter>
                    </FormalParameter>
                    <DisplayName>1.ecuContext.f.f</DisplayName>
                    <OID>UKN_XFD34BJ0J1024TAKC</OID>
                  </ActualParameter>
                </ActualParameter>
                <IncomingTransition>
                  <LocalReference oid="UKN_XH294BJ0J0042T0B8" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_XT294BJ0J00YOH62K" type="Transition" />
                </OutgoingTransition>
                <OID>UKN_XED34BJ0J00U3WTC9</OID>
              </StructureNode>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>6</ConditionOrder>
                <Condition>command.equals(Commands.CODE)</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XC4O6BJ0J005Z6DEO" type="Transition" />
                </IncomingTransition>
                <DisplayName>CODE</DisplayName>
                <OID>UKN_XB4O6BJ0J00NQTKMJ</OID>
              </DecisionCondition>
              <DecisionCondition>
                <ConditionMode>exit</ConditionMode>
                <ConditionOrder>7</ConditionOrder>
                <Condition>command.equals(Commands.FLASH)</Condition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XFDT6BJ0J00RWA2YB" type="Transition" />
                </IncomingTransition>
                <DisplayName>FLASH</DisplayName>
                <OID>UKN_XEDT6BJ0J00VH923F</OID>
              </DecisionCondition>
            </Activity>
            <OID>UKN_XX8VXJECIHCRTBYTX</OID>
          </Chart>
        </Chart>
        <FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X5CHSRVHIY4PGXKV4" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Assign the assembly check test function here.&#xD;
Removed because of references</Data>
                <OID>UKN_XAWSSRVHI183SM9A6</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XAWSSRVHI08THD5SS</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X2WF5KECIHCYC5NDY" type="DecisionCondition" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>When using KLine communication, add in ReleaseSemaphore.bb</Data>
                <OID>UKN_XUNTYAT0J10GK06BX</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XUNTYAT0J00S29F5T</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XT927KECIHFTK8FA5" type="DecisionCondition" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>When using KLine communication, add in ReleaseSemaphore.bb directly after TF_EcuConnectionClose.</Data>
                <OID>UKN_XO1RZAT0J10MEYWQZ</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XO1RZAT0J00TIZ6S6</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XTYWYJECIUWU9US2M" type="BuildingBlock" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Switch to the ODX Perspective (Click on the ODX Icon on the upper right corner).  Select the ECU on the left side, and use drag and drop to put it above [ECURuntimeDataGet].&#xD;
&#xD;
Select [EcuRuntimeDataGet] and assign the newly created ECU Link</Data>
                <OID>UKN_XLKMKJU0J10GWKUUG</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XLKMKJU0J00R8I44N</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X42PJRECIINCF0KBN" type="Cts" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Reads the list of DTC's from the Logical Link. The logicalLink used in EcuRuntimeDataGetWithLocation needs to be used here. Additionally, the Statistic Context is assigned here.&#xD;
&#xD;
Optionally assign a RTDtcList element for the retrieved list of DTCs. </Data>
                <OID>UKN_XUVWELU0J107WUJQ7</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XUVWELU0J00CEY288</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XROFORECI7YNBPGG0" type="Cts" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Clears the DTCs for the Logical Link provided. &#xD;
&#xD;
The logicalLink used in EcuRuntimeDataGetWithLocation needs to be used here. Additionally, the Statistic Context is assigned here.</Data>
                <OID>UKN_XRZ2LLU0J10BDPWTD</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XRZ2LLU0J00W6IZ84</OID>
          </FloatingComment>
        </FloatingComment>
        <LocalVariable>
          <ComplexExternalValue>
            <Blobvalue>
              <Blobdata>
                <OID>UKN_XCAOBBJ0J10IT56VG</OID>
              </Blobdata>
            </Blobvalue>
            <DataType>
              <Reference oid="SDT_ILocation" type="ComplexExternalDataType" />
            </DataType>
            <DisplayName>ecu</DisplayName>
            <Description>ILocation retrieved from the ODX browser</Description>
            <OID>UKN_XCAOBBJ0J00X8K189</OID>
          </ComplexExternalValue>
          <StructValue>
            <StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0J10O3LJQW</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>id</DisplayName>
                    <OID>UKN_XXJ0RG37I9K44BZNH</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0J004KO0DK</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0J30OR7GVA</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>logicalLink</DisplayName>
                    <OID>UKN_X01YVG37IM2YBNRID</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0J208C3FEY</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0J5074WP3R</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>partNumber</DisplayName>
                    <OID>UKN_X30WX978I4UDWLRI4</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0J407I1FG0</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0J703D2D27</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>softwareVersion</DisplayName>
                    <OID>UKN_X6GLY978I7UOHX05P</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0J60LP4USH</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0J90OVWDH2</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>hardwareVersion</DisplayName>
                    <OID>UKN_X7XRY978IAUUSEYUQ</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0J80LOADGQ</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0JB08HMI4B</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>AsamFileID</DisplayName>
                    <OID>UKN_XL0NHA78IWLG7DLKI</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JA09E75ZB</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0JD04USCLT</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>AsamFileVersion</DisplayName>
                    <OID>UKN_XKQTHA78IZL7RXR13</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JC0NFPGUG</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0JF0RVR3FY</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>hardwareReferencePartNumber</DisplayName>
                    <OID>UKN_XPMIOA78ILMH9IO18</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JE0K57T7C</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0JH0CM9CLQ</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>referencePartNumber</DisplayName>
                    <OID>UKN_X6RWLD19IWYS9LHG6</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JG0HDR5DC</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XM7BJJU0JJ03PSG0V</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>serialNumber</DisplayName>
                    <OID>UKN_X2CVND19IC6THTSVY</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JI0R5WEQ0</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <OID>UKN_XM7BJJU0JL00S1C2I</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>logicalLinkName</DisplayName>
                    <OID>UKN_X66A8N9QI00ORZECU</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JK0RFRSKN</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <OID>UKN_XM7BJJU0JN0EPR2VW</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>locationName</DisplayName>
                    <OID>UKN_XIKO8N9QI00TWZJ0B</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JM0DGQ4UW</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <OID>UKN_XM7BJJU0JP076G35E</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>ecuVariantName</DisplayName>
                    <OID>UKN_XYFC9N9QI00GAF9R4</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XM7BJJU0JO06N4BJ0</OID>
              </StructElement>
            </StructElement>
            <DataType>
              <Reference oid="SDT_XXJ0RG37I8KJRICX3" type="StructDataType" />
            </DataType>
            <DisplayName>ecuRuntimeData</DisplayName>
            <Description>The SDT that contains the runtime information for the tested ECU</Description>
            <OID>UKN_XJNSZJECIF23XVU9G</OID>
          </StructValue>
          <Variable>
            <DataType>
              <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
            </DataType>
            <DisplayName>logicalLink</DisplayName>
            <Description>The logical link that will be tested.</Description>
            <OID>UKN_XMNSZJECII2XRZBBJ</OID>
          </Variable>
          <StructValue>
            <StructElement>
              <StructElement>
                <DataElement>
                  <SymbolValue>
                    <FormalParameterSymbol>
                      <FormalParameter>
                        <ParameterDirection>in</ParameterDirection>
                        <OrderNo>1</OrderNo>
                        <DataType>
                          <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                        </DataType>
                        <DisplayName>modelContext</DisplayName>
                        <Description>The statistics model used to display statistics</Description>
                        <OID>UKN_XC8HMPECIJKX4SWXP</OID>
                      </FormalParameter>
                    </FormalParameterSymbol>
                    <DataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XQ8RJJU0J10OQKDJ7</OID>
                  </SymbolValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>parent</DisplayName>
                    <Description>The parent context</Description>
                    <OID>UKN_MEMBERPARENT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XQ8RJJU0J00Y7IVA5</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <StatisticContextValue>
                    <StatisticContext>
                      <Reference oid="STC_XR0R7IJJI50HDDIL8" type="StatisticContext" />
                    </StatisticContext>
                    <DataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </DataType>
                    <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                    <OID>UKN_XQ8RJJU0J30IAG04F</OID>
                  </StatisticContextValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>statisticContextEntity</DisplayName>
                    <Description>The statistic context</Description>
                    <OID>UKN_MEMBERSTATCNTXT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XQ8RJJU0J20GWFRYA</OID>
              </StructElement>
            </StructElement>
            <DataType>
              <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
            </DataType>
            <DisplayName>ecuContext</DisplayName>
            <Description>Statistics context for ECU or distributed function</Description>
            <OID>UKN_X334PPECIN74SOIZH</OID>
          </StructValue>
          <Variable>
            <Value>de.dsa.[customer].[ecu].[module_name]</Value>
            <DataType>
              <Reference oid="SDT_ILogger" type="ExternalDataType" />
            </DataType>
            <DisplayName>LOG</DisplayName>
            <Description>ILogger variable, path has to be adapted</Description>
            <OID>UKN_X3MJKMVHIYHIQ544V</OID>
          </Variable>
        </LocalVariable>
        <OID>CTc_XX8VXJECIGCVWDIB4</OID>
        <Completeness>incomplete</Completeness>
        <Description>Variant</Description>
        <RestrictionStatus>UNKNOWN</RestrictionStatus>
      </CtsClassVariant>
    </CtsClassVariant>
    <FormalParameter>
      <FormalParameter>
        <ParameterDirection>in</ParameterDirection>
        <OrderNo>0</OrderNo>
        <DataType>
          <Reference oid="SDT_XSCMXUXZI30G1S3JP" type="AliasDataType" />
        </DataType>
        <DisplayName>command</DisplayName>
        <Description>The command/state to be executed in the handler</Description>
        <OID>UKN_X05ORKECIWQIE7Y7W</OID>
      </FormalParameter>
      <LocalReference oid="UKN_XC8HMPECIJKX4SWXP" type="FormalParameter" />
    </FormalParameter>
    <ScriptingCategory>testmodule</ScriptingCategory>
    <TestModuleAcceptanceStatus>no_status</TestModuleAcceptanceStatus>
    <VisualizationType>standard</VisualizationType>
    <DisplayName>TM_[base_variant_name]_ECUHandler</DisplayName>
    <Description>TODO:&#xD;
&#xD;
- Get ILocation from ODX Browser for TF_ConnectionOpen&#xD;
- Add the correct ECU Statistic Context&#xD;
- Fix iLogger&#xD;
- Assign TF_AssemblyCheck (removed to prevent circular references)&#xD;
- Change the description&#xD;
&#xD;
NOTE: When KLine communication is used, the element "ReleaseSemaphore.bb" needs to be added to:&#xD;
- CONNECTION_OPEN&#xD;
- CONNECTION_CLOSE (Directly after TF_EcuConnectionClose)</Description>
    <Completeness>complete</Completeness>
    <OID>CTc_XX8VXJECISCPLX8QO</OID>
  </CtsClass>
</xmlExporter>

