<?xml version="1.0" encoding="UTF-8"?>
<xmlExporter version="14.0.0">
  <CtsClass>
    <CtsClassVariant>
      <CtsClassVariant>
        <Chart>
          <Chart>
            <Activity>
              <StartNode>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XRIAHXFCI65N4KV9C" />
                    <Target OID="UKN_XRIAHXFCI851BTAPD" />
                    <OID>UKN_XRIAHXFCI75GPJ2VN</OID>
                  </Transition>
                </OutgoingTransition>
                <OID>UKN_XRIAHXFCI65N4KV9C</OID>
              </StartNode>
              <EndNode>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_XVIAHXFCIR9FCXDRA" />
                    <Target OID="UKN_XWIAHXFCIFACZTSF9" />
                    <OID>UKN_XWIAHXFCIEA6F9I8T</OID>
                  </Transition>
                </IncomingTransition>
                <DisplayName>End</DisplayName>
                <OID>UKN_XWIAHXFCIFACZTSF9</OID>
              </EndNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XRIAHXFCIB5IH1U2A" />
                            <Target OID="UKN_XC22TXFCIGVW4N70C" />
                            <OID>UKN_XNXOSANCIQ6P2AYE3</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XRIAHXFCIB5IH1U2A</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_XSIAHXFCIR52NOVMS" />
                            <Target OID="UKN_XSIAHXFCIB6NIOD9U" />
                            <OID>UKN_XSIAHXFCIA6MOGF2K</OID>
                          </Transition>
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XSIAHXFCIB6NIOD9U</OID>
                      </CompoundOut>
                      <BuildingBlock>
                        <BuildingBlockClass>
                          <Reference oid="BBC_X64PN7U3I0CU5FYDE" type="BuildingBlockClass" />
                        </BuildingBlockClass>
                        <ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XHMRTXFCIM1U8RN9Y" type="Variable" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <OrderNo>0</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                                </DataType>
                                <DisplayName>logicalLink</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_XK5ZN7U3IEC5Y8AV8</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <DisplayName>1.logicalLink.f.f</DisplayName>
                            <OID>UKN_XSIAHXFCIS5LGLJZ9</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XSIAHXFCIW5GJ25OE" type="Variable" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <OrderNo>1</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>status</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_XCRAO7U3IICWQSWXJ</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <DisplayName>1.status.f.f</DisplayName>
                            <OID>UKN_XSIAHXFCIV54DIPOL</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XVZ9YYT0J004NWJO8" type="Constant" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <OrderNo>2</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>action</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_X850P7U3I8WVRNDWX</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <DisplayName>1.action.f.f</DisplayName>
                            <OID>UKN_XSIAHXFCI16Z0Z56Y</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <IsOptional>true</IsOptional>
                                <OrderNo>3</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>progress</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_XTF0PLV3I42528O7N</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>0</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_XSIAHXFCI5691SRB6</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.progress.f.f</DisplayName>
                            <OID>UKN_XSIAHXFCI46JJMBM5</OID>
                          </ActualParameter>
                        </ActualParameter>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_XC22TXFCIGVW4N70C" />
                            <Target OID="UKN_XSIAHXFCIR52NOVMS" />
                            <OID>UKN_XTA7I37SI00AXTSB0</OID>
                          </Transition>
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_XSIAHXFCIA6MOGF2K" type="Transition" />
                        </OutgoingTransition>
                        <OID>UKN_XSIAHXFCIR52NOVMS</OID>
                      </BuildingBlock>
                      <BuildingBlock>
                        <BuildingBlockClass>
                          <Reference oid="BBC_X14Q5LP9IQRCYBL7H" type="BuildingBlockClass" />
                        </BuildingBlockClass>
                        <ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XN05HZT0J00MHB5FD" type="ComplexExternalValue" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>in</ParameterDirection>
                                <OrderNo>0</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_ILocation" type="ComplexExternalDataType" />
                                </DataType>
                                <DisplayName>location</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_X24Q5LP9IXR57WO4C</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <DisplayName>1.location.f.f</DisplayName>
                            <OID>UKN_XC22TXFCIJVT7O033</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XOF9VXFCIN8I8KLFS" type="StructValue" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>out</ParameterDirection>
                                <OrderNo>1</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_XXJ0RG37I8KJRICX3" type="StructDataType" />
                                </DataType>
                                <DisplayName>ecuRuntime</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_X24Q5LP9I0SS2G6CT</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <DisplayName>2.ecuRuntime.f.f</DisplayName>
                            <OID>UKN_XC22TXFCIMVAJWSCR</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XHMRTXFCIM1U8RN9Y" type="Variable" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <FormalParameter>
                                <ParameterDirection>out</ParameterDirection>
                                <OrderNo>2</OrderNo>
                                <DataType>
                                  <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                                </DataType>
                                <DisplayName>logicalLink</DisplayName>
                                <Description>&lt;DESCRIPTION NOT LOADED&gt;</Description>
                                <OID>UKN_X24Q5LP9I3SCUAADF</OID>
                              </FormalParameter>
                            </FormalParameter>
                            <DisplayName>2.logicalLink.f.f</DisplayName>
                            <OID>UKN_XC22TXFCIPVS85G8B</OID>
                          </ActualParameter>
                        </ActualParameter>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XNXOSANCIQ6P2AYE3" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_XTA7I37SI00AXTSB0" type="Transition" />
                        </OutgoingTransition>
                        <OID>UKN_XC22TXFCIGVW4N70C</OID>
                      </BuildingBlock>
                    </Activity>
                    <OID>UKN_XRIAHXFCI95VDN6MD</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XRIAHXFCI75GPJ2VN" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XRIAHXFCI851BTAPD" />
                    <Target OID="UKN_X7I0NXFCINIKPR9HB" />
                    <OID>UKN_X2E1NXFCIYIU9J356</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Initialization</DisplayName>
                <OID>UKN_XRIAHXFCI851BTAPD</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>none</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XVIAHXFCIU9PH41WR" />
                            <Target OID="UKN_XWIAHXFCI7AX9E5DF" />
                            <OID>UKN_XK4AI37SI00FZHPPE</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XVIAHXFCIU9PH41WR</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XK4AI37SI00FZHPPE" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XWIAHXFCI7AX9E5DF</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XVIAHXFCIS9LQIOJ3</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_X3F2YYT0J10Y4QVA4" />
                    <Target OID="UKN_XVIAHXFCIR9FCXDRA" />
                    <OID>UKN_XVEFYYT0J0094YMCV</OID>
                  </Transition>
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_XWIAHXFCIEA6F9I8T" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Finalization</DisplayName>
                <OID>UKN_XVIAHXFCIR9FCXDRA</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X7I0NXFCIRIKFYWQ2" />
                            <Target OID="UKN_XN458ZT0J00O3BEGW" />
                            <OID>UKN_X3C78ZT0J00AS7EX5</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X7I0NXFCIRIKFYWQ2</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_XN458ZT0J10M333LJ" />
                            <Target OID="UKN_X7I0NXFCIUIQREYWD" />
                            <OID>UKN_X3C78ZT0J10H79UBA</OID>
                          </Transition>
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X7I0NXFCIUIQREYWD</OID>
                      </CompoundOut>
                      <StructureNode>
                        <AssessmentAction>none</AssessmentAction>
                        <AssessmentMode>direct</AssessmentMode>
                        <Chart>
                          <Chart>
                            <Activity>
                              <CompoundIn>
                                <OutgoingTransition>
                                  <Transition>
                                    <Source OID="UKN_XN458ZT0JA0DFPQCV" />
                                    <Target OID="UKN_XX458ZT0J005DWECS" />
                                    <OID>UKN_XX458ZT0J10LT520X</OID>
                                  </Transition>
                                </OutgoingTransition>
                                <OID>UKN_XN458ZT0JA0DFPQCV</OID>
                              </CompoundIn>
                              <CompoundOut>
                                <IncomingTransition>
                                  <Transition>
                                    <Source OID="UKN_XX458ZT0J30Y36SGQ" />
                                    <Target OID="UKN_XX458ZT0J40YKH7OB" />
                                    <OID>UKN_XX458ZT0J50FILZS1</OID>
                                  </Transition>
                                </IncomingTransition>
                                <OID>UKN_XX458ZT0J40YKH7OB</OID>
                              </CompoundOut>
                              <StructureNode>
                                <AssessmentAction>none</AssessmentAction>
                                <AssessmentMode>direct</AssessmentMode>
                                <Chart>
                                  <Chart>
                                    <Activity>
                                      <CompoundIn>
                                        <OutgoingTransition>
                                          <Transition>
                                            <Source OID="UKN_XX458ZT0JA004W0JX" />
                                            <Target OID="UKN_XX458ZT0JB0VNDENA" />
                                            <OID>UKN_XX458ZT0JC0W3ASOM</OID>
                                          </Transition>
                                        </OutgoingTransition>
                                        <OID>UKN_XX458ZT0JA004W0JX</OID>
                                      </CompoundIn>
                                      <CompoundOut>
                                        <IncomingTransition>
                                          <LocalReference oid="UKN_XX458ZT0JC0W3ASOM" type="Transition" />
                                        </IncomingTransition>
                                        <OID>UKN_XX458ZT0JB0VNDENA</OID>
                                      </CompoundOut>
                                    </Activity>
                                    <OID>UKN_XX458ZT0J80YNDBTZ</OID>
                                  </Chart>
                                </Chart>
                                <CondExecCondition>ok</CondExecCondition>
                                <CondExecMode>always</CondExecMode>
                                <AssessmentCondition>0</AssessmentCondition>
                                <IncomingTransition>
                                  <LocalReference oid="UKN_XX458ZT0J10LT520X" type="Transition" />
                                </IncomingTransition>
                                <OutgoingTransition>
                                  <Transition>
                                    <Source OID="UKN_XX458ZT0J005DWECS" />
                                    <Target OID="UKN_XX458ZT0J20VRWQFW" />
                                    <OID>UKN_XX458ZT0J709UZTYU</OID>
                                  </Transition>
                                </OutgoingTransition>
                                <DisplayName>--- Drag and drop your service execution ---</DisplayName>
                                <OID>UKN_XX458ZT0J005DWECS</OID>
                              </StructureNode>
                              <StructureNode>
                                <AssessmentAction>none</AssessmentAction>
                                <AssessmentMode>direct</AssessmentMode>
                                <Chart>
                                  <Chart>
                                    <Activity>
                                      <CompoundIn>
                                        <OutgoingTransition>
                                          <Transition>
                                            <Source OID="UKN_XX458ZT0JF0Q2ZJPZ" />
                                            <Target OID="UKN_XX458ZT0JG0GYJJBS" />
                                            <OID>UKN_XX458ZT0JH0XV03I9</OID>
                                          </Transition>
                                        </OutgoingTransition>
                                        <OID>UKN_XX458ZT0JF0Q2ZJPZ</OID>
                                      </CompoundIn>
                                      <CompoundOut>
                                        <IncomingTransition>
                                          <LocalReference oid="UKN_XX458ZT0JH0XV03I9" type="Transition" />
                                        </IncomingTransition>
                                        <OID>UKN_XX458ZT0JG0GYJJBS</OID>
                                      </CompoundOut>
                                    </Activity>
                                    <OID>UKN_XX458ZT0JD0DMGP4Y</OID>
                                  </Chart>
                                </Chart>
                                <CondExecCondition>ok</CondExecCondition>
                                <CondExecMode>always</CondExecMode>
                                <AssessmentCondition>0</AssessmentCondition>
                                <IncomingTransition>
                                  <LocalReference oid="UKN_XX458ZT0J709UZTYU" type="Transition" />
                                </IncomingTransition>
                                <OutgoingTransition>
                                  <Transition>
                                    <Source OID="UKN_XX458ZT0J20VRWQFW" />
                                    <Target OID="UKN_XX458ZT0J30Y36SGQ" />
                                    <OID>UKN_XX458ZT0J60D9HFWO</OID>
                                  </Transition>
                                </OutgoingTransition>
                                <DisplayName>--- Select your evaluation strategy ---</DisplayName>
                                <OID>UKN_XX458ZT0J20VRWQFW</OID>
                              </StructureNode>
                              <StructureNode>
                                <AssessmentAction>none</AssessmentAction>
                                <AssessmentMode>direct</AssessmentMode>
                                <BuildingBlockClass>
                                  <Reference oid="BBC_X64PN7U3I0CU5FYDE" type="BuildingBlockClass" />
                                </BuildingBlockClass>
                                <Chart>
                                  <Chart>
                                    <Activity>
                                      <CompoundIn>
                                        <OutgoingTransition>
                                          <Transition>
                                            <Source OID="UKN_XX458ZT0JP08C20LU" />
                                            <Target OID="UKN_XX458ZT0JQ0AYA7QC" />
                                            <OID>UKN_XX458ZT0JR08IZLV4</OID>
                                          </Transition>
                                        </OutgoingTransition>
                                        <OID>UKN_XX458ZT0JP08C20LU</OID>
                                      </CompoundIn>
                                      <CompoundOut>
                                        <IncomingTransition>
                                          <LocalReference oid="UKN_XX458ZT0JR08IZLV4" type="Transition" />
                                        </IncomingTransition>
                                        <OID>UKN_XX458ZT0JQ0AYA7QC</OID>
                                      </CompoundOut>
                                    </Activity>
                                    <OID>UKN_XX458ZT0JN0HXABOM</OID>
                                  </Chart>
                                </Chart>
                                <CondExecCondition>ok</CondExecCondition>
                                <CondExecMode>always</CondExecMode>
                                <AssessmentCondition>0</AssessmentCondition>
                                <ActualParameter>
                                  <ActualParameter>
                                    <DataElementSymbol>
                                      <LocalReference oid="UKN_XHMRTXFCIM1U8RN9Y" type="Variable" />
                                    </DataElementSymbol>
                                    <FormalParameter>
                                      <LocalReference oid="UKN_XK5ZN7U3IEC5Y8AV8" type="FormalParameter" />
                                    </FormalParameter>
                                    <DisplayName>1.logicalLink.f.f</DisplayName>
                                    <OID>UKN_XX458ZT0JI0AKC2RB</OID>
                                  </ActualParameter>
                                  <ActualParameter>
                                    <DataElementSymbol>
                                      <LocalReference oid="UKN_XSIAHXFCIW5GJ25OE" type="Variable" />
                                    </DataElementSymbol>
                                    <FormalParameter>
                                      <LocalReference oid="UKN_XCRAO7U3IICWQSWXJ" type="FormalParameter" />
                                    </FormalParameter>
                                    <DisplayName>1.status.f.f</DisplayName>
                                    <OID>UKN_XX458ZT0JJ0KM95GO</OID>
                                  </ActualParameter>
                                  <ActualParameter>
                                    <DataElementSymbol>
                                      <LocalReference oid="UKN_XVZ9YYT0J004NWJO8" type="Constant" />
                                    </DataElementSymbol>
                                    <FormalParameter>
                                      <LocalReference oid="UKN_X850P7U3I8WVRNDWX" type="FormalParameter" />
                                    </FormalParameter>
                                    <DisplayName>1.action.f.f</DisplayName>
                                    <OID>UKN_XX458ZT0JK0CA8U4F</OID>
                                  </ActualParameter>
                                  <ActualParameter>
                                    <FormalParameter>
                                      <LocalReference oid="UKN_XTF0PLV3I42528O7N" type="FormalParameter" />
                                    </FormalParameter>
                                    <ValueSymbol>
                                      <DataElementValue>
                                        <Value>50</Value>
                                        <DataType>
                                          <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                        </DataType>
                                        <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                        <OID>UKN_XX458ZT0JM05C07CY</OID>
                                      </DataElementValue>
                                    </ValueSymbol>
                                    <DisplayName>1.progress.f.f</DisplayName>
                                    <OID>UKN_XX458ZT0JL0016NR9</OID>
                                  </ActualParameter>
                                </ActualParameter>
                                <IncomingTransition>
                                  <LocalReference oid="UKN_XX458ZT0J60D9HFWO" type="Transition" />
                                </IncomingTransition>
                                <OutgoingTransition>
                                  <LocalReference oid="UKN_XX458ZT0J50FILZS1" type="Transition" />
                                </OutgoingTransition>
                                <Description>This element calls the .fireUpdate() method of the ecuOverviewEvent. This sends an Event to UI_EcuOverview containing the information to be updated on the UI_EcuOverview screen.</Description>
                                <OID>UKN_XX458ZT0J30Y36SGQ</OID>
                              </StructureNode>
                            </Activity>
                            <OID>UKN_XN458ZT0J80TS85LW</OID>
                          </Chart>
                        </Chart>
                        <CondExecCondition>ok</CondExecCondition>
                        <CondExecMode>always</CondExecMode>
                        <AssessmentCondition>0</AssessmentCondition>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X3C78ZT0J00AS7EX5" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XN458ZT0J00O3BEGW" />
                            <Target OID="UKN_XN458ZT0J10M333LJ" />
                            <OID>UKN_XN458ZT0J20MTFL2M</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>(Sample) Measure: Voltage between 10 and 14</DisplayName>
                        <OID>UKN_XN458ZT0J00O3BEGW</OID>
                      </StructureNode>
                      <StructureNode>
                        <AssessmentAction>none</AssessmentAction>
                        <AssessmentMode>direct</AssessmentMode>
                        <Chart>
                          <Chart>
                            <Activity>
                              <CompoundIn>
                                <OutgoingTransition>
                                  <Transition>
                                    <Source OID="UKN_XN458ZT0J50QB9537" />
                                    <Target OID="UKN_XN458ZT0J601IGO4B" />
                                    <OID>UKN_XN458ZT0J70FOJRLU</OID>
                                  </Transition>
                                </OutgoingTransition>
                                <OID>UKN_XN458ZT0J50QB9537</OID>
                              </CompoundIn>
                              <CompoundOut>
                                <IncomingTransition>
                                  <LocalReference oid="UKN_XN458ZT0J70FOJRLU" type="Transition" />
                                </IncomingTransition>
                                <OID>UKN_XN458ZT0J601IGO4B</OID>
                              </CompoundOut>
                            </Activity>
                            <OID>UKN_XN458ZT0J30Z9A9ST</OID>
                          </Chart>
                        </Chart>
                        <CondExecCondition>ok</CondExecCondition>
                        <CondExecMode>always</CondExecMode>
                        <AssessmentCondition>0</AssessmentCondition>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XN458ZT0J20MTFL2M" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_X3C78ZT0J10H79UBA" type="Transition" />
                        </OutgoingTransition>
                        <DisplayName>--- copy the previous step as many times as necessary ---</DisplayName>
                        <OID>UKN_XN458ZT0J10M333LJ</OID>
                      </StructureNode>
                    </Activity>
                    <OID>UKN_X7I0NXFCIQI054TXZ</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X2E1NXFCIYIU9J356" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X7I0NXFCINIKPR9HB" />
                    <Target OID="UKN_X3F2YYT0J00WDF5KV" />
                    <OID>UKN_XJ1AYYT0J008IP6XD</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>[--insert test here--]</DisplayName>
                <OID>UKN_X7I0NXFCINIKPR9HB</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>none</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X3F2YYT0JI0RLV2K0" />
                            <Target OID="UKN_X3F2YYT0JJ0IGXLZT" />
                            <OID>UKN_X3F2YYT0JK0V2RB8C</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3F2YYT0JI0RLV2K0</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_X3F2YYT0JJ0IGXLZT" />
                            <Target OID="UKN_X3F2YYT0JL0S4TCS7" />
                            <OID>UKN_X3F2YYT0JM0TCNU8C</OID>
                          </Transition>
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3F2YYT0JL0S4TCS7</OID>
                      </CompoundOut>
                      <BuildingBlock>
                        <BuildingBlockClass>
                          <Reference oid="BBC_X64PN7U3I0CU5FYDE" type="BuildingBlockClass" />
                        </BuildingBlockClass>
                        <ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XHMRTXFCIM1U8RN9Y" type="Variable" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <LocalReference oid="UKN_XK5ZN7U3IEC5Y8AV8" type="FormalParameter" />
                            </FormalParameter>
                            <DisplayName>1.logicalLink.f.f</DisplayName>
                            <OID>UKN_X3F2YYT0JN04TPF7H</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XCRAO7U3IICWQSWXJ" type="FormalParameter" />
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>OK</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_X3F2YYT0JP0YA4GZ1</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.status.f.f</DisplayName>
                            <OID>UKN_X3F2YYT0JO0OWN04P</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XVZ9YYT0J004NWJO8" type="Constant" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <LocalReference oid="UKN_X850P7U3I8WVRNDWX" type="FormalParameter" />
                            </FormalParameter>
                            <DisplayName>1.action.f.f</DisplayName>
                            <OID>UKN_XDF2YYT0J00M4IMX3</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XTF0PLV3I42528O7N" type="FormalParameter" />
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>100</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_XDF2YYT0J20U6VYX6</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.progress.f.f</DisplayName>
                            <OID>UKN_XDF2YYT0J10MI5NIU</OID>
                          </ActualParameter>
                        </ActualParameter>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X3F2YYT0JK0V2RB8C" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_X3F2YYT0JM0TCNU8C" type="Transition" />
                        </OutgoingTransition>
                        <DisplayName>Report OK</DisplayName>
                        <OID>UKN_X3F2YYT0JJ0IGXLZT</OID>
                      </BuildingBlock>
                    </Activity>
                    <OID>UKN_X3F2YYT0JG01PGWHN</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>if</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XJ1AYYT0J008IP6XD" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X3F2YYT0J00WDF5KV" />
                    <Target OID="UKN_X3F2YYT0J10Y4QVA4" />
                    <OID>UKN_X3F2YYT0J20AB4PNG</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test: OK</DisplayName>
                <OID>UKN_X3F2YYT0J00WDF5KV</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>none</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X3F2YYT0J509DL1PM" />
                            <Target OID="UKN_X3F2YYT0J60OJUEO5" />
                            <OID>UKN_X3F2YYT0J70HGT6C8</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3F2YYT0J509DL1PM</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <Transition>
                            <Source OID="UKN_X3F2YYT0J60OJUEO5" />
                            <Target OID="UKN_X3F2YYT0J80ZDW1N2" />
                            <OID>UKN_X3F2YYT0J90SERROL</OID>
                          </Transition>
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3F2YYT0J80ZDW1N2</OID>
                      </CompoundOut>
                      <BuildingBlock>
                        <BuildingBlockClass>
                          <Reference oid="BBC_X64PN7U3I0CU5FYDE" type="BuildingBlockClass" />
                        </BuildingBlockClass>
                        <ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XHMRTXFCIM1U8RN9Y" type="Variable" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <LocalReference oid="UKN_XK5ZN7U3IEC5Y8AV8" type="FormalParameter" />
                            </FormalParameter>
                            <DisplayName>1.logicalLink.f.f</DisplayName>
                            <OID>UKN_X3F2YYT0JA0LIZ6SV</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XCRAO7U3IICWQSWXJ" type="FormalParameter" />
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>NOK</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_X3F2YYT0JC0ET7XYO</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.status.f.f</DisplayName>
                            <OID>UKN_X3F2YYT0JB0DQNPWQ</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <DataElementSymbol>
                              <LocalReference oid="UKN_XVZ9YYT0J004NWJO8" type="Constant" />
                            </DataElementSymbol>
                            <FormalParameter>
                              <LocalReference oid="UKN_X850P7U3I8WVRNDWX" type="FormalParameter" />
                            </FormalParameter>
                            <DisplayName>1.action.f.f</DisplayName>
                            <OID>UKN_X3F2YYT0JD07KV3GW</OID>
                          </ActualParameter>
                          <ActualParameter>
                            <FormalParameter>
                              <LocalReference oid="UKN_XTF0PLV3I42528O7N" type="FormalParameter" />
                            </FormalParameter>
                            <ValueSymbol>
                              <DataElementValue>
                                <Value>100</Value>
                                <DataType>
                                  <Reference oid="SDT_0000000000000100" type="MslBasicDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_X3F2YYT0JF0WKOF16</OID>
                              </DataElementValue>
                            </ValueSymbol>
                            <DisplayName>1.progress.f.f</DisplayName>
                            <OID>UKN_X3F2YYT0JE07MIQZJ</OID>
                          </ActualParameter>
                        </ActualParameter>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X3F2YYT0J70HGT6C8" type="Transition" />
                        </IncomingTransition>
                        <OutgoingTransition>
                          <LocalReference oid="UKN_X3F2YYT0J90SERROL" type="Transition" />
                        </OutgoingTransition>
                        <DisplayName>Report NOK</DisplayName>
                        <OID>UKN_X3F2YYT0J60OJUEO5</OID>
                      </BuildingBlock>
                    </Activity>
                    <OID>UKN_X3F2YYT0J30TC1FVV</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>prog</CondExecCondition>
                <CondExecMode>if</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <CondExecCode>returnValues.getReturnValue() != OK</CondExecCode>
                <IncomingTransition>
                  <LocalReference oid="UKN_X3F2YYT0J20AB4PNG" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_XVEFYYT0J0094YMCV" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Test: Not OK</DisplayName>
                <OID>UKN_X3F2YYT0J10Y4QVA4</OID>
              </StructureNode>
            </Activity>
            <OID>UKN_XRIAHXFCI45CK0UGJ</OID>
          </Chart>
        </Chart>
        <FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XRIAHXFCI851BTAPD" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>In the initialisation part, the ECU Overview is updated with the name of performed test and the information that the test is in progress. Furthermore, the SDT_ECURuntimeData object is retrieved from memory for use in the test module.</Data>
                <OID>UKN_XUJCEYT0J108STHVE</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XUJCEYT0J00TE1CWP</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X7I0NXFCINIKPR9HB" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Structure your Test&#xD;
&#xD;
Enter the section [-- insert Test Here--].  Use Structure nodes to create the steps of your sequence.  As an example:&#xD;
&#xD;
- Measure: Voltage between 10 &amp; 14&#xD;
- Actuator Test: Activate Horn&#xD;
- Measure: Door Locked&#xD;
&#xD;
The created structure must help you understand the structure of the sequence directly, and simplifies reordering associated elements&#xD;
&#xD;
You can simply copy and paste the structure node as many times as you need to have a complete test instruction</Data>
                <OID>UKN_X1VJXYT0J10LB2XYG</OID>
              </Blobdata>
            </Text>
            <OID>UKN_X1VJXYT0J00P63B3Y</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XVIAHXFCIR9FCXDRA" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Optional, can be removed if not needed.</Data>
                <OID>UKN_XJ96ZYT0J10G8CASE</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XJ96ZYT0J00RW1FVM</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X3F2YYT0J00WDF5KV" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Test Result&#xD;
&#xD;
In this structure node you can execute actions that are not part of the actual test anymore, but depend on the result of the test, such as updating an overview for the user. It is important to assure that the return value of the whole test module still reflects the result of the test. This has to be assured by setting the assessment of the structure node to none for both cases and also a possible finalization part.</Data>
                <OID>UKN_XN7OZYT0J1021GMHW</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XN7OZYT0J00L2KTDS</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X3F2YYT0JJ0IGXLZT" type="BuildingBlock" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Provide User Feedback&#xD;
&#xD;
You can use the UI_EcuOverviewFireUpdateEventWithLogicalLink to provide progress indications to the users.  You can update the progress bar after each step&#xD;
&#xD;
It is also recommended to change the constant "ACTUATOR_TEST" to a representative name for the whole test.  This is used to provide user feedback on the ECU Overview.  The string used will be loaded as an image, if it exists.</Data>
                <OID>UKN_XD5P0ZT0J10CJF2WQ</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XD5P0ZT0J00W0TC81</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XX458ZT0J005DWECS" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Assign MCD Services&#xD;
&#xD;
Switch to the ODX Perspective, and search for the services required for the test.&#xD;
&#xD;
For each test step (as structured above), open the structure node and drag and drop the service inside.   Assign the logical link.  Double click on the service execution building block and assign parameters.&#xD;
&#xD;
In the case that a response value must be evaluated, assign a variable "toleranceMeasurement"</Data>
                <OID>UKN_X9WV8ZT0J10JKKWKV</OID>
              </Blobdata>
            </Text>
            <OID>UKN_X9WV8ZT0J00FTTZPE</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XX458ZT0J20VRWQFW" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Select your Evaluation Strategy&#xD;
&#xD;
For diagnostic steps that require evaluation, you can add an evaluation strategy after the diagnostics step&#xD;
&#xD;
Typical Evaluation strategies are:&#xD;
&#xD;
Loop Evaluation&#xD;
Visual Check&#xD;
...</Data>
                <OID>UKN_X55Q9ZT0J10PQPTDJ</OID>
              </Blobdata>
            </Text>
            <OID>UKN_X55Q9ZT0J00F9NAA4</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XX458ZT0J30Y36SGQ" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Provide User Feedback&#xD;
&#xD;
You can use the UI_EcuOverviewFireUpdateEventWithLogicalLink to provide progress indications to the users.  You can update the progress bar after each step&#xD;
&#xD;
It is also recommended to change the constant "ACTUATOR_TEST" to a representative name for the whole test.  This is used to provide user feedback on the ECU Overview.  The string used will be loaded as an image, if it exists.</Data>
                <OID>UKN_X1KDBZT0J104CIU3A</OID>
              </Blobdata>
            </Text>
            <OID>UKN_X1KDBZT0J00QGT0T3</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X7I0NXFCINIKPR9HB" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Add Statistics&#xD;
&#xD;
A Test Module (TM) Must contain the following statistics:&#xD;
&#xD;
Model Context as Formal Parameter:  The model context is received from the outside as a formal parameter.  As ECUs can be build on several vehicle types, it should not be hardcoded here&#xD;
&#xD;
ECU / Distributed Function Context as Data Element:  Open the Statistic Hierarchy explorer and drag and drop a Statistic Context of Level "ECU" for the ECU that is tested here.  The parent of this Statistic Context must be the Model Context&#xD;
&#xD;
Function Context:  Open the Statistic Hierarchy explorer and drag and drop a Statistic Context of Level "FUNCTION" that represents the test performed in this test module.  The parent of this Statistic Context must be the ECU Context&#xD;
&#xD;
Statistic Steps&#xD;
Typically one step is required for each Test Step.  That means, each evaluation strategy will be associated with a statistic step. It may be necesssary to also add statistics to individual service executions.&#xD;
&#xD;
Drag and drop the desired statistics from the Statistics hierarchy explorer, and associate them to your evaluation strategies.</Data>
                <OID>UKN_X9PTCZT0J1067WH4D</OID>
              </Blobdata>
            </Text>
            <OID>UKN_X9PTCZT0J00UD2D54</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XC22TXFCIGVW4N70C" type="BuildingBlock" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>ECU Connection Preparation&#xD;
&#xD;
Open the "Initialization" block with a double click.  Switch to the ODX Perspective (Click on the ODX Icon on the upper right corner).  Select the ECU on the left side, and use drag and drop to put it above [ECURuntimeDataGet].&#xD;
&#xD;
Select [EcuRuntimeDataGet] and assign the newly created ECU Link</Data>
                <OID>UKN_XBCYDZT0J105BMTWV</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XBCYDZT0J00ILH7IU</OID>
          </FloatingComment>
          <FloatingComment>
            <Text>
              <Blobdata>
                <Data>ODX Context Assignment&#xD;
&#xD;
Go to the ODX Tab in the bottom.  Assign a vehicle type and a ECU Variant Group&#xD;
&#xD;
This step will associate your test sequence with an ECU from an ODX Project, and will enable to drag and drop ODX Services</Data>
                <OID>UKN_X9VKKZT0J1051FAU8</OID>
              </Blobdata>
            </Text>
            <OID>UKN_X9VKKZT0J000WFX9Z</OID>
          </FloatingComment>
        </FloatingComment>
        <LocalVariable>
          <StructValue>
            <StructElement>
              <StructElement>
                <DataElement>
                  <SymbolValue>
                    <DataElementSymbol>
                      <StructValue>
                        <StructElement>
                          <StructElement>
                            <DataElement>
                              <SymbolValue>
                                <FormalParameterSymbol>
                                  <FormalParameter>
                                    <ParameterDirection>in</ParameterDirection>
                                    <OrderNo>0</OrderNo>
                                    <DataType>
                                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                                    </DataType>
                                    <DisplayName>modelContext</DisplayName>
                                    <Description>Model context for the vehicle to be tested.</Description>
                                    <OID>UKN_X7IPUXFCII28E22F2</OID>
                                  </FormalParameter>
                                </FormalParameterSymbol>
                                <DataType>
                                  <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                                </DataType>
                                <DisplayName>NO DISPLAY NAME</DisplayName>
                                <OID>UKN_X2F0XZT0J109B4P5R</OID>
                              </SymbolValue>
                            </DataElement>
                            <ElementType>
                              <StructMember>
                                <MemberDataType>
                                  <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                                </MemberDataType>
                                <IsTermAllowed>true</IsTermAllowed>
                                <DisplayName>parent</DisplayName>
                                <Description>The parent context</Description>
                                <OID>UKN_MEMBERPARENT</OID>
                              </StructMember>
                            </ElementType>
                            <OID>UKN_X2F0XZT0J00H1T106</OID>
                          </StructElement>
                          <StructElement>
                            <DataElement>
                              <StatisticContextValue>
                                <StatisticContext>
                                  <Reference oid="STC_XR0R7IJJI50HDDIL8" type="StatisticContext" />
                                </StatisticContext>
                                <DataType>
                                  <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                                </DataType>
                                <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                                <OID>UKN_X2F0XZT0J3006C2LD</OID>
                              </StatisticContextValue>
                            </DataElement>
                            <ElementType>
                              <StructMember>
                                <MemberDataType>
                                  <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                                </MemberDataType>
                                <IsTermAllowed>true</IsTermAllowed>
                                <DisplayName>statisticContextEntity</DisplayName>
                                <Description>The statistic context</Description>
                                <OID>UKN_MEMBERSTATCNTXT</OID>
                              </StructMember>
                            </ElementType>
                            <OID>UKN_X2F0XZT0J20H38RF8</OID>
                          </StructElement>
                        </StructElement>
                        <DataType>
                          <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                        </DataType>
                        <DisplayName>ecuContext</DisplayName>
                        <Description>Statistics context for ECU or distributed function</Description>
                        <OID>UKN_XQ7GUXFCIZ1QG7G2A</OID>
                      </StructValue>
                    </DataElementSymbol>
                    <DataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_X9PQFZT0J10ACP9NT</OID>
                  </SymbolValue>
                </DataElement>
                <ElementType>
                  <LocalReference oid="UKN_MEMBERPARENT" type="StructMember" />
                </ElementType>
                <OID>UKN_X9PQFZT0J00M4WZOZ</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <StatisticContextValue>
                    <StatisticContext>
                      <Reference oid="STC_XY4S6IJJI50KB0B63" type="StatisticContext" />
                    </StatisticContext>
                    <DataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </DataType>
                    <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                    <OID>UKN_X9PQFZT0J7054ICKY</OID>
                  </StatisticContextValue>
                </DataElement>
                <ElementType>
                  <LocalReference oid="UKN_MEMBERSTATCNTXT" type="StructMember" />
                </ElementType>
                <OID>UKN_X9PQFZT0J60LJDKL3</OID>
              </StructElement>
            </StructElement>
            <DataType>
              <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
            </DataType>
            <DisplayName>functionContext</DisplayName>
            <Description>Function context for the test performed by this module</Description>
            <OID>UKN_XTIAHXFCIT6JVDM6I</OID>
          </StructValue>
          <Variable>
            <Value>IN_PROGRESS</Value>
            <DataType>
              <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
            </DataType>
            <DisplayName>IN_PROGRESS</DisplayName>
            <Description>Can be sent to ECU overview to provide feedback that the test is currently being performed</Description>
            <OID>UKN_XSIAHXFCIW5GJ25OE</OID>
          </Variable>
          <Variable>
            <DataType>
              <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
            </DataType>
            <DisplayName>logicalLink</DisplayName>
            <Description>The logical link that will be tested.</Description>
            <OID>UKN_XHMRTXFCIM1U8RN9Y</OID>
          </Variable>
          <LocalReference oid="UKN_XQ7GUXFCIZ1QG7G2A" type="StructValue" />
          <ComplexExternalValue>
            <Blobvalue>
              <Blobdata>
                <OID>UKN_XN05HZT0J10XEXIBR</OID>
              </Blobdata>
            </Blobvalue>
            <DataType>
              <Reference oid="SDT_ILocation" type="ComplexExternalDataType" />
            </DataType>
            <DisplayName>location</DisplayName>
            <Description>Ilocation retrieved from the ODX browser</Description>
            <OID>UKN_XN05HZT0J00MHB5FD</OID>
          </ComplexExternalValue>
          <StructValue>
            <StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JA4F1ML4G</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>id</DisplayName>
                    <OID>UKN_XXJ0RG37I9K44BZNH</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0J941Y1LRA</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JC4781XVE</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_RTLogicalLink" type="ExternalDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>logicalLink</DisplayName>
                    <OID>UKN_X01YVG37IM2YBNRID</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JB4BU9N5K</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JE497NWE4</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>partNumber</DisplayName>
                    <OID>UKN_X30WX978I4UDWLRI4</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JD42YE3XF</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JG4033HUU</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>softwareVersion</DisplayName>
                    <OID>UKN_X6GLY978I7UOHX05P</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JF4PRZOM7</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JI4I81CUQ</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>hardwareVersion</DisplayName>
                    <OID>UKN_X7XRY978IAUUSEYUQ</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JH4BLYFD6</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JK4470KAF</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>AsamFileID</DisplayName>
                    <OID>UKN_XL0NHA78IWLG7DLKI</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JJ4M7BCVZ</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JM4L2IQB9</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>AsamFileVersion</DisplayName>
                    <OID>UKN_XKQTHA78IZL7RXR13</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JL4FMNFE3</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JO4URKYFQ</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>hardwareReferencePartNumber</DisplayName>
                    <OID>UKN_XPMIOA78ILMH9IO18</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JN43P3KP0</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JQ4F1ESVK</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>referencePartNumber</DisplayName>
                    <OID>UKN_X6RWLD19IWYS9LHG6</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JP4RYH7MY</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <DisplayName>NO DISPLAY NAME</DisplayName>
                    <OID>UKN_XP1Y90U0JS4BI1WW1</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>serialNumber</DisplayName>
                    <OID>UKN_X2CVND19IC6THTSVY</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JR4NQ9GUM</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <OID>UKN_XP1Y90U0JU4O7A6D3</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>logicalLinkName</DisplayName>
                    <OID>UKN_X66A8N9QI00ORZECU</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JT4XWD5UY</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <OID>UKN_XP1Y90U0JW4PHOOU3</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>locationName</DisplayName>
                    <OID>UKN_XIKO8N9QI00TWZJ0B</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JV4VP5YP9</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <DataElementValue>
                    <DataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </DataType>
                    <OID>UKN_XP1Y90U0JY4K6J1PO</OID>
                  </DataElementValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>ecuVariantName</DisplayName>
                    <OID>UKN_XYFC9N9QI00GAF9R4</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XP1Y90U0JX4M6OI8O</OID>
              </StructElement>
            </StructElement>
            <DataType>
              <Reference oid="SDT_XXJ0RG37I8KJRICX3" type="StructDataType" />
            </DataType>
            <DisplayName>ecuRuntime</DisplayName>
            <Description>SDT that contains the runtime information for the tested ECU</Description>
            <OID>UKN_XOF9VXFCIN8I8KLFS</OID>
          </StructValue>
          <Variable>
            <Value>de.dsa.[customer].[ecu].[module_name]</Value>
            <DataType>
              <Reference oid="SDT_ILogger" type="ExternalDataType" />
            </DataType>
            <DisplayName>LOG</DisplayName>
            <Description>ILogger variable, path has to be adapted</Description>
            <OID>UKN_XMW3NYFCI0ZD4IVCU</OID>
          </Variable>
          <Constant>
            <IsConstant>true</IsConstant>
            <Value>13_Actuator</Value>
            <DataType>
              <Reference oid="SDT_0000000000000102" type="MslBasicDataType" />
            </DataType>
            <DisplayName>ACTUATOR_TEST</DisplayName>
            <Description>Example for a test module identifier</Description>
            <OID>UKN_XVZ9YYT0J004NWJO8</OID>
          </Constant>
        </LocalVariable>
        <OID>CTc_XRIAHXFCI255RCFH0</OID>
        <Completeness>incomplete</Completeness>
        <Description>Variant</Description>
        <RestrictionStatus>UNKNOWN</RestrictionStatus>
      </CtsClassVariant>
    </CtsClassVariant>
    <FormalParameter>
      <LocalReference oid="UKN_X7IPUXFCII28E22F2" type="FormalParameter" />
    </FormalParameter>
    <ScriptingCategory>testmodule</ScriptingCategory>
    <TestModuleAcceptanceStatus>no_status</TestModuleAcceptanceStatus>
    <VisualizationType>standard</VisualizationType>
    <DisplayName>TM_[base_variant_name]_[module_name]</DisplayName>
    <Description>Template for ECU-Specific Test Modules&#xD;
Test Modules implement parts of a Commissioning Instruction.&#xD;
&#xD;
Follow the instructions on the comments tab to fully implement this Test Module</Description>
    <Completeness>complete</Completeness>
    <OID>CTc_XRIAHXFCIS454780Z</OID>
  </CtsClass>
</xmlExporter>

