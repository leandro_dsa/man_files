<?xml version="1.0" encoding="UTF-8"?>
<xmlExporter version="14.0.0">
  <CtsClass>
    <CtsClassVariant>
      <CtsClassVariant>
        <Chart>
          <Chart>
            <Activity>
              <StartNode>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XFOTJRFCI569XVD5U" />
                    <Target OID="UKN_X58AQRFCIRFF989FE" />
                    <OID>UKN_X6729MVHI4EFKT1EA</OID>
                  </Transition>
                </OutgoingTransition>
                <OID>UKN_XFOTJRFCI569XVD5U</OID>
              </StartNode>
              <EndNode>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_XNBMRRFCIDLA63CM4" />
                    <Target OID="UKN_XFOTJRFCI86C43G79" />
                    <OID>UKN_X57A9MVHILEJFMH5Y</OID>
                  </Transition>
                </IncomingTransition>
                <DisplayName>End</DisplayName>
                <OID>UKN_XFOTJRFCI86C43G79</OID>
              </EndNode>
              <Fork>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_X54DQRFCIHG44CBNM" />
                    <Target OID="UKN_X900PRFCIUCNM1347" />
                    <OID>UKN_X7729MVHI5EFOA8HX</OID>
                  </Transition>
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X900PRFCIUCNM1347" />
                    <Target OID="UKN_X2KHRRFCI3R3QSU8M" />
                    <OID>UKN_X9Y49MVHI8EM62KOK</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_X900PRFCIUCNM1347" />
                    <Target OID="UKN_XVNKRRFCI5BV7BNEQ" />
                    <OID>UKN_X4V69MVHICE6G3M34</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_X900PRFCIUCNM1347" />
                    <Target OID="UKN_XO3JRRFCI41ISVO6Z" />
                    <OID>UKN_X4H89MVHIGEMHIQW6</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Fork - Parallel Tasks</DisplayName>
                <OID>UKN_X900PRFCIUCNM1347</OID>
              </Fork>
              <Join>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_X3KHRRFCIARIFOW0G" />
                    <Target OID="UKN_XUR8PRFCIZDZE7619" />
                    <OID>UKN_XAY49MVHI9ENW2R5F</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XVNKRRFCICBUVWBEO" />
                    <Target OID="UKN_XUR8PRFCIZDZE7619" />
                    <OID>UKN_X6V69MVHIDEOCHBU3</OID>
                  </Transition>
                  <Transition>
                    <Source OID="UKN_XO3JRRFCIB1D9L3IX" />
                    <Target OID="UKN_XUR8PRFCIZDZE7619" />
                    <OID>UKN_X5H89MVHIHEEE092A</OID>
                  </Transition>
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XUR8PRFCIZDZE7619" />
                    <Target OID="UKN_XNBMRRFCI6LFRYV20" />
                    <OID>UKN_X57A9MVHIKE9YM2QE</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Join - Wait for Parallel Tasks</DisplayName>
                <OID>UKN_XUR8PRFCIZDZE7619</OID>
              </Join>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X58AQRFCIVFPAYM6G" />
                            <Target OID="UKN_X58AQRFCIYFQYBJKG" />
                            <OID>UKN_X58AQRFCI1GMJDSSJ</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X58AQRFCIVFPAYM6G</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X58AQRFCI1GMJDSSJ" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X58AQRFCIYFQYBJKG</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X58AQRFCIUF6PJFF8</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X6729MVHI4EFKT1EA" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X58AQRFCIRFF989FE" />
                    <Target OID="UKN_X6MBQRFCI4GQD5BSD" />
                    <OID>UKN_X09CQRFCIFGT8RJEH</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_OPEN</DisplayName>
                <OID>UKN_X58AQRFCIRFF989FE</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X6MBQRFCI8G154BRR" />
                            <Target OID="UKN_X6MBQRFCIBG7GUYK6" />
                            <OID>UKN_X6MBQRFCIEGH7MTLR</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X6MBQRFCI8G154BRR</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X6MBQRFCIEGH7MTLR" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X6MBQRFCIBG7GUYK6</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X6MBQRFCI7GN77IRZ</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X09CQRFCIFGT8RJEH" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X6MBQRFCI4GQD5BSD" />
                    <Target OID="UKN_X54DQRFCIHG44CBNM" />
                    <OID>UKN_XLKDQRFCISGQZYJTM</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: COMMAND</DisplayName>
                <OID>UKN_X6MBQRFCI4GQD5BSD</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X54DQRFCILGNSZ745" />
                            <Target OID="UKN_X54DQRFCIOGO5E85A" />
                            <OID>UKN_X54DQRFCIRG22MB74</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X54DQRFCILGNSZ745</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X54DQRFCIRG22MB74" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X54DQRFCIOGO5E85A</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X54DQRFCIKGQ5LASH</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XLKDQRFCISGQZYJTM" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_X7729MVHI5EFOA8HX" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_CLOSE</DisplayName>
                <OID>UKN_X54DQRFCIHG44CBNM</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X3KHRRFCILR21C8TH" />
                            <Target OID="UKN_X3KHRRFCIORAHQ16K" />
                            <OID>UKN_X3KHRRFCIRR5QMDOO</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3KHRRFCILR21C8TH</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X3KHRRFCIRR5QMDOO" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3KHRRFCIORAHQ16K</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X3KHRRFCIJRR2MCN6</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X9Y49MVHI8EM62KOK" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X2KHRRFCI3R3QSU8M" />
                    <Target OID="UKN_X2KHRRFCI7RMX5OW3" />
                    <OID>UKN_X3KHRRFCIIRZ93OC5</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_OPEN</DisplayName>
                <OID>UKN_X2KHRRFCI3R3QSU8M</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X3KHRRFCIURB81IQ9" />
                            <Target OID="UKN_X3KHRRFCIXRCJJWBB" />
                            <OID>UKN_X3KHRRFCI0S8VSTLE</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3KHRRFCIURB81IQ9</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X3KHRRFCI0S8VSTLE" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X3KHRRFCIXRCJJWBB</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X3KHRRFCISRLQYQM8</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X3KHRRFCIIRZ93OC5" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_X2KHRRFCI7RMX5OW3" />
                    <Target OID="UKN_X3KHRRFCIARIFOW0G" />
                    <OID>UKN_X3KHRRFCIHRZAYUFP</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: COMMAND</DisplayName>
                <OID>UKN_X2KHRRFCI7RMX5OW3</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_X4KHRRFCI3S8LDYTO" />
                            <Target OID="UKN_X4KHRRFCI6STC5MD8" />
                            <OID>UKN_X4KHRRFCI9SBBIHIG</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X4KHRRFCI3S8LDYTO</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X4KHRRFCI9SBBIHIG" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X4KHRRFCI6STC5MD8</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_X4KHRRFCI1S2G99DX</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X3KHRRFCIHRZAYUFP" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_XAY49MVHI9ENW2R5F" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_CLOSE</DisplayName>
                <OID>UKN_X3KHRRFCIARIFOW0G</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XWNKRRFCINBO74A90" />
                            <Target OID="UKN_XWNKRRFCIQB603753" />
                            <OID>UKN_XWNKRRFCITBDMGL36</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XWNKRRFCINBO74A90</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XWNKRRFCITBDMGL36" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XWNKRRFCIQB603753</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XWNKRRFCILBE8WKBM</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X4V69MVHICE6G3M34" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XVNKRRFCI5BV7BNEQ" />
                    <Target OID="UKN_XVNKRRFCI9BKS01C0" />
                    <OID>UKN_XWNKRRFCIKBXRL0H9</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_OPEN</DisplayName>
                <OID>UKN_XVNKRRFCI5BV7BNEQ</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XWNKRRFCIWB3RAYQO" />
                            <Target OID="UKN_XWNKRRFCIZB1CTHX2" />
                            <OID>UKN_XWNKRRFCI2CLPN70Q</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XWNKRRFCIWB3RAYQO</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XWNKRRFCI2CLPN70Q" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XWNKRRFCIZB1CTHX2</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XWNKRRFCIUBEYSO50</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XWNKRRFCIKBXRL0H9" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XVNKRRFCI9BKS01C0" />
                    <Target OID="UKN_XVNKRRFCICBUVWBEO" />
                    <OID>UKN_XWNKRRFCIJBXI8ZRF</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: COMMAND</DisplayName>
                <OID>UKN_XVNKRRFCI9BKS01C0</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XWNKRRFCI5CMDE53C" />
                            <Target OID="UKN_XWNKRRFCI8CKNT3PK" />
                            <OID>UKN_XWNKRRFCIBCXZ8CK7</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XWNKRRFCI5CMDE53C</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XWNKRRFCIBCXZ8CK7" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XWNKRRFCI8CKNT3PK</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XWNKRRFCI3C6H3XR9</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XWNKRRFCIJBXI8ZRF" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_X6V69MVHIDEOCHBU3" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_CLOSE</DisplayName>
                <OID>UKN_XVNKRRFCICBUVWBEO</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XO3JRRFCIM1RCKQQT" />
                            <Target OID="UKN_XO3JRRFCIP1V4ASIW" />
                            <OID>UKN_XO3JRRFCIS149ZEKP</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XO3JRRFCIM1RCKQQT</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XO3JRRFCIS149ZEKP" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XO3JRRFCIP1V4ASIW</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XO3JRRFCIK16XJ73S</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X4H89MVHIGEMHIQW6" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XO3JRRFCI41ISVO6Z" />
                    <Target OID="UKN_XO3JRRFCI81GEC24P" />
                    <OID>UKN_XO3JRRFCIJ18O29UX</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_OPEN</DisplayName>
                <OID>UKN_XO3JRRFCI41ISVO6Z</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XP3JRRFCIV1JHSXCH" />
                            <Target OID="UKN_XP3JRRFCIY1RJDJ80" />
                            <OID>UKN_XP3JRRFCI122AIAHW</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XP3JRRFCIV1JHSXCH</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XP3JRRFCI122AIAHW" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XP3JRRFCIY1RJDJ80</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XP3JRRFCIT1UXM36M</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XO3JRRFCIJ18O29UX" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XO3JRRFCI81GEC24P" />
                    <Target OID="UKN_XO3JRRFCIB1D9L3IX" />
                    <OID>UKN_XO3JRRFCII1CIRG64</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: COMMAND</DisplayName>
                <OID>UKN_XO3JRRFCI81GEC24P</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XP3JRRFCI42S993B3" />
                            <Target OID="UKN_XP3JRRFCI72UQP7VR" />
                            <OID>UKN_XP3JRRFCIA2N25ZKO</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XP3JRRFCI42S993B3</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XP3JRRFCIA2N25ZKO" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XP3JRRFCI72UQP7VR</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XP3JRRFCI223SRUP2</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XO3JRRFCII1CIRG64" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_X5H89MVHIHEEE092A" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_CLOSE</DisplayName>
                <OID>UKN_XO3JRRFCIB1D9L3IX</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XOBMRRFCIOLTRFH2A" />
                            <Target OID="UKN_XOBMRRFCIRLB171KS" />
                            <OID>UKN_XOBMRRFCIULUVH2AF</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XOBMRRFCIOLTRFH2A</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XOBMRRFCIULUVH2AF" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XOBMRRFCIRLB171KS</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XOBMRRFCIMLQOUMPJ</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_X57A9MVHIKE9YM2QE" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XNBMRRFCI6LFRYV20" />
                    <Target OID="UKN_XNBMRRFCIALIWMFOL" />
                    <OID>UKN_XOBMRRFCILL3M9BM8</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_OPEN</DisplayName>
                <OID>UKN_XNBMRRFCI6LFRYV20</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XPBMRRFCIXLPQ8F54" />
                            <Target OID="UKN_XPBMRRFCI0MB7YZ3U" />
                            <OID>UKN_XPBMRRFCI3M6Q4O76</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XPBMRRFCIXLPQ8F54</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XPBMRRFCI3M6Q4O76" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XPBMRRFCI0MB7YZ3U</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XPBMRRFCIVLSA1X1W</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XOBMRRFCILL3M9BM8" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XNBMRRFCIALIWMFOL" />
                    <Target OID="UKN_XNBMRRFCIDLA63CM4" />
                    <OID>UKN_XOBMRRFCIKLU6D4GC</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Test Module Call: COMMAND</DisplayName>
                <OID>UKN_XNBMRRFCIALIWMFOL</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XPBMRRFCI6M3LUD4J" />
                            <Target OID="UKN_XPBMRRFCI9MQN8CA2" />
                            <OID>UKN_XPBMRRFCICMP63F0D</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XPBMRRFCI6M3LUD4J</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XPBMRRFCICMP63F0D" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XPBMRRFCI9MQN8CA2</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XPBMRRFCI4M990JSC</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <AssessmentCondition>0</AssessmentCondition>
                <IncomingTransition>
                  <LocalReference oid="UKN_XOBMRRFCIKLU6D4GC" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_X57A9MVHILEJFMH5Y" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Test Module Call: CONNECTION_CLOSE</DisplayName>
                <OID>UKN_XNBMRRFCIDLA63CM4</OID>
              </StructureNode>
            </Activity>
            <OID>UKN_XFOTJRFCI46Q6X7I5</OID>
          </Chart>
        </Chart>
        <FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X58AQRFCIRFF989FE" type="StructureNode" />
              <LocalReference oid="UKN_X2KHRRFCI3R3QSU8M" type="StructureNode" />
              <LocalReference oid="UKN_XO3JRRFCI41ISVO6Z" type="StructureNode" />
              <LocalReference oid="UKN_XVNKRRFCI5BV7BNEQ" type="StructureNode" />
              <LocalReference oid="UKN_XNBMRRFCI6LFRYV20" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Call Test Module and Open Connection to the ECU by setting command to "CONNECTION_OPEN"</Data>
                <OID>UKN_XCULEOU0J10UWNSE7</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XCULEOU0J00CH7CD4</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X6MBQRFCI4GQD5BSD" type="StructureNode" />
              <LocalReference oid="UKN_X2KHRRFCI7RMX5OW3" type="StructureNode" />
              <LocalReference oid="UKN_XO3JRRFCI81GEC24P" type="StructureNode" />
              <LocalReference oid="UKN_XVNKRRFCI9BKS01C0" type="StructureNode" />
              <LocalReference oid="UKN_XNBMRRFCIALIWMFOL" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Call Test Module with specific command. This may be "ASSEMBLY_CHECK", "READ_DTC", "DELETE_DTC", "FLASH" or "CODE"</Data>
                <OID>UKN_XNMFKPU0J105MIAOT</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XNMFKPU0J00A5NAYB</OID>
          </FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_X54DQRFCIHG44CBNM" type="StructureNode" />
              <LocalReference oid="UKN_X3KHRRFCIARIFOW0G" type="StructureNode" />
              <LocalReference oid="UKN_XO3JRRFCIB1D9L3IX" type="StructureNode" />
              <LocalReference oid="UKN_XVNKRRFCICBUVWBEO" type="StructureNode" />
              <LocalReference oid="UKN_XNBMRRFCIDLA63CM4" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Call Test Module and Close Connection to the ECU by setting command to "CONNECTION_CLOSE"</Data>
                <OID>UKN_XIASNPU0J10DBGK4O</OID>
              </Blobdata>
            </Text>
            <OID>UKN_XIASNPU0J00G40UUJ</OID>
          </FloatingComment>
        </FloatingComment>
        <LocalVariable>
          <StructValue>
            <StructElement>
              <StructElement>
                <DataElement>
                  <SymbolValue>
                    <DataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </DataType>
                    <OID>UKN_X6HJYNU0J10JMLPWR</OID>
                  </SymbolValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>parent</DisplayName>
                    <Description>The parent context</Description>
                    <OID>UKN_MEMBERPARENT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_X6HJYNU0J00KA8528</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <StatisticContextValue>
                    <StatisticContext>
                      <Reference oid="STC_XZBS4IJJI50X393E0" type="StatisticContext" />
                    </StatisticContext>
                    <DataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </DataType>
                    <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                    <OID>UKN_X6HJYNU0J30EP1PB1</OID>
                  </StatisticContextValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>statisticContextEntity</DisplayName>
                    <Description>The statistic context</Description>
                    <OID>UKN_MEMBERSTATCNTXT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_X6HJYNU0J202MR35A</OID>
              </StructElement>
            </StructElement>
            <DataType>
              <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
            </DataType>
            <DisplayName>ctxModelContextDummy</DisplayName>
            <OID>UKN_X90M5IJJI00C2ZJIE</OID>
          </StructValue>
        </LocalVariable>
        <OID>CTc_XFOTJRFCI36LDU2X5</OID>
        <Completeness>incomplete</Completeness>
        <Description>Variant</Description>
        <RestrictionStatus>UNKNOWN</RestrictionStatus>
      </CtsClassVariant>
    </CtsClassVariant>
    <ScriptingCategory>testplan</ScriptingCategory>
    <TestModuleAcceptanceStatus>no_status</TestModuleAcceptanceStatus>
    <VisualizationType>standard</VisualizationType>
    <DisplayName>TP_[test_plan_name]_[model_name]</DisplayName>
    <Description>TODO:&#xD;
&#xD;
- Fill Test Plan with tasks that need to be performed&#xD;
- Add Model context for Statistics&#xD;
- Fix this Description, describe the functions performed in this test plan.</Description>
    <Completeness>complete</Completeness>
    <OID>CTc_XFOTJRFCIF6409Q4P</OID>
  </CtsClass>
</xmlExporter>

