<?xml version="1.0" encoding="UTF-8"?>
<xmlExporter version="14.0.0">
  <CtsClass>
    <CtsClassVariant>
      <CtsClassVariant>
        <Chart>
          <Chart>
            <Activity>
              <StartNode>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XSARAXFCILRX1BLKM" />
                    <Target OID="UKN_XB67BXFCIPGQ4WRUY" />
                    <OID>UKN_XG6CQANCI4ZA3PADT</OID>
                  </Transition>
                </OutgoingTransition>
                <OID>UKN_XSARAXFCILRX1BLKM</OID>
              </StartNode>
              <EndNode>
                <IncomingTransition>
                  <Transition>
                    <Source OID="UKN_XZMABXFCITH54DNRL" />
                    <Target OID="UKN_XZARAXFCI8Z1P7Z82" />
                    <OID>UKN_XD1DQANCIJZGVR24D</OID>
                  </Transition>
                </IncomingTransition>
                <DisplayName>End</DisplayName>
                <OID>UKN_XZARAXFCI8Z1P7Z82</OID>
              </EndNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XB67BXFCITGLA29MQ" />
                            <Target OID="UKN_XB67BXFCIWGGGU49B" />
                            <OID>UKN_XB67BXFCIZGTCNUFC</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XB67BXFCITGLA29MQ</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XB67BXFCIZGTCNUFC" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XB67BXFCIWGGGU49B</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XB67BXFCISGJ91P94</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <ConditionalValidity>
                  <Reference oid="LAS_XHWV3MVHI58QD87PP" type="Las" />
                </ConditionalValidity>
                <AssessmentCondition>0</AssessmentCondition>
                <WillEvaluateValidity>true</WillEvaluateValidity>
                <IncomingTransition>
                  <LocalReference oid="UKN_XG6CQANCI4ZA3PADT" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XB67BXFCIPGQ4WRUY" />
                    <Target OID="UKN_XC09BXFCIGHVELZA7" />
                    <OID>UKN_XZQ9BXFCIRH1WVEJT</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Call Test Plan for Model A</DisplayName>
                <OID>UKN_XB67BXFCIPGQ4WRUY</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XC09BXFCIKHOS5165" />
                            <Target OID="UKN_XC09BXFCINHOT5OMG" />
                            <OID>UKN_XC09BXFCIQHE986L0</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XC09BXFCIKHOS5165</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_XC09BXFCIQHE986L0" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XC09BXFCINHOT5OMG</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XC09BXFCIJHG6GYFI</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <ConditionalValidity>
                  <Reference oid="LAS_XTF34MVHIJ8JZZQA6" type="Las" />
                </ConditionalValidity>
                <AssessmentCondition>0</AssessmentCondition>
                <WillEvaluateValidity>true</WillEvaluateValidity>
                <IncomingTransition>
                  <LocalReference oid="UKN_XZQ9BXFCIRH1WVEJT" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <Transition>
                    <Source OID="UKN_XC09BXFCIGHVELZA7" />
                    <Target OID="UKN_XZMABXFCITH54DNRL" />
                    <OID>UKN_XS3BBXFCI4I7TGOIM</OID>
                  </Transition>
                </OutgoingTransition>
                <DisplayName>Call Test Plan for Model B</DisplayName>
                <OID>UKN_XC09BXFCIGHVELZA7</OID>
              </StructureNode>
              <StructureNode>
                <AssessmentAction>none</AssessmentAction>
                <AssessmentMode>direct</AssessmentMode>
                <Chart>
                  <Chart>
                    <Activity>
                      <CompoundIn>
                        <OutgoingTransition>
                          <Transition>
                            <Source OID="UKN_XZMABXFCIXHT6CU5V" />
                            <Target OID="UKN_X0NABXFCI0I5PC9OH" />
                            <OID>UKN_X0NABXFCI3IZFSGXS</OID>
                          </Transition>
                        </OutgoingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_XZMABXFCIXHT6CU5V</OID>
                      </CompoundIn>
                      <CompoundOut>
                        <IncomingTransition>
                          <LocalReference oid="UKN_X0NABXFCI3IZFSGXS" type="Transition" />
                        </IncomingTransition>
                        <DisplayName>NO DISPLAY NAME</DisplayName>
                        <OID>UKN_X0NABXFCI0I5PC9OH</OID>
                      </CompoundOut>
                    </Activity>
                    <OID>UKN_XZMABXFCIWHX52J13</OID>
                  </Chart>
                </Chart>
                <CondExecCondition>ok</CondExecCondition>
                <CondExecMode>always</CondExecMode>
                <ConditionalValidity>
                  <Reference oid="LAS_XQX94MVHIX89BQHCR" type="Las" />
                </ConditionalValidity>
                <AssessmentCondition>0</AssessmentCondition>
                <WillEvaluateValidity>true</WillEvaluateValidity>
                <IncomingTransition>
                  <LocalReference oid="UKN_XS3BBXFCI4I7TGOIM" type="Transition" />
                </IncomingTransition>
                <OutgoingTransition>
                  <LocalReference oid="UKN_XD1DQANCIJZGVR24D" type="Transition" />
                </OutgoingTransition>
                <DisplayName>Call Test Plan for Model C</DisplayName>
                <OID>UKN_XZMABXFCITH54DNRL</OID>
              </StructureNode>
            </Activity>
            <OID>UKN_XSARAXFCIJRK95VMG</OID>
          </Chart>
        </Chart>
        <FloatingComment>
          <FloatingComment>
            <LinkedActivity>
              <LocalReference oid="UKN_XB67BXFCIPGQ4WRUY" type="StructureNode" />
              <LocalReference oid="UKN_XC09BXFCIGHVELZA7" type="StructureNode" />
              <LocalReference oid="UKN_XZMABXFCITH54DNRL" type="StructureNode" />
            </LinkedActivity>
            <Text>
              <Blobdata>
                <Data>Example LAS conditional executions per model</Data>
                <OID>UKN_X7FG6MVHICC4JWAU0</OID>
              </Blobdata>
            </Text>
            <OID>UKN_X7FG6MVHIBCW95UAP</OID>
          </FloatingComment>
        </FloatingComment>
        <LocalVariable>
          <StructValue>
            <StructElement>
              <StructElement>
                <DataElement>
                  <SymbolValue>
                    <DataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </DataType>
                    <OID>UKN_XYXR7NU0J10RQM5I9</OID>
                  </SymbolValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>parent</DisplayName>
                    <Description>The parent context</Description>
                    <OID>UKN_MEMBERPARENT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XYXR7NU0J003YQU52</OID>
              </StructElement>
              <StructElement>
                <DataElement>
                  <StatisticContextValue>
                    <StatisticContext>
                      <Reference oid="STC_XZBS4IJJI50X393E0" type="StatisticContext" />
                    </StatisticContext>
                    <DataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </DataType>
                    <DisplayName>&lt;&lt;FPARAMVALUE&gt;&gt;</DisplayName>
                    <OID>UKN_XYXR7NU0J30XM8V2N</OID>
                  </StatisticContextValue>
                </DataElement>
                <ElementType>
                  <StructMember>
                    <MemberDataType>
                      <Reference oid="SDT_0000000000000200" type="DbDataStreamDataType" />
                    </MemberDataType>
                    <IsTermAllowed>true</IsTermAllowed>
                    <DisplayName>statisticContextEntity</DisplayName>
                    <Description>The statistic context</Description>
                    <OID>UKN_MEMBERSTATCNTXT</OID>
                  </StructMember>
                </ElementType>
                <OID>UKN_XYXR7NU0J20310JFP</OID>
              </StructElement>
            </StructElement>
            <DataType>
              <Reference oid="SDT_STRUCTSTATCONTEXT" type="StructDataType" />
            </DataType>
            <DisplayName>ctxModelContextDummy</DisplayName>
            <OID>UKN_XXS55IJJI009PAIL2</OID>
          </StructValue>
        </LocalVariable>
        <OID>CTc_XSARAXFCIHR06QH4N</OID>
        <Completeness>incomplete</Completeness>
        <Description>Variant</Description>
        <RestrictionStatus>UNKNOWN</RestrictionStatus>
      </CtsClassVariant>
    </CtsClassVariant>
    <ScriptingCategory>testplan</ScriptingCategory>
    <TestModuleAcceptanceStatus>no_status</TestModuleAcceptanceStatus>
    <VisualizationType>standard</VisualizationType>
    <DisplayName>TP_[test_plan_name]_Location</DisplayName>
    <Description>TODO:&#xD;
&#xD;
- Fill Test Plan with Test Plans for all model(s) that are built&#xD;
- Change LAS examples for MODEL A,B,C / Other conditions to filter execution of the various models if required&#xD;
- Add Model context for Statistics&#xD;
- Change the description</Description>
    <Completeness>complete</Completeness>
    <OID>CTc_XSARAXFCI7R65MX3B</OID>
  </CtsClass>
</xmlExporter>

